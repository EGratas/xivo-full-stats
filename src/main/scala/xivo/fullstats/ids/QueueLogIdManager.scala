package xivo.fullstats.ids

import java.sql.Connection

import xivo.fullstats.EventParser
import xivo.fullstats.model.QueueLog

class QueueLogIdManager(implicit c: Connection) extends EventParser[QueueLog] {
  override def parseEvent(e: QueueLog): Unit = QueueLog.setLastId(e.id)

  override def saveState(): Unit = ()
}
