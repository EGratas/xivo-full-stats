package xivo.fullstats.ids

import java.sql.Connection

import xivo.fullstats.EventParser
import xivo.fullstats.model.Cel

class CelIdManager(implicit c: Connection) extends EventParser[Cel] {
  override def parseEvent(e: Cel): Unit = Cel.setLastId(e.id)

  override def saveState(): Unit = ()
}
