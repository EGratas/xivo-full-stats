package xivo.fullstats

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import org.joda.time.Period
import org.slf4j.LoggerFactory
import xivo.fullstats.Main.appConf
import xivo.fullstats.agent.{AgentPositionManager, StatAgentManager}
import xivo.fullstats.callparsers.{CallDataGenerator, CallOnQueueGenerator}
import xivo.fullstats.ids.{CelIdManager, QueueLogIdManager}
import xivo.fullstats.iterators.{CallEventIterator, ClosingHolder}
import xivo.fullstats.model._
import xivo.fullstats.queue.StatQueueManager

import scala.concurrent.duration._
import scala.util.Try

class XivoStat(factory: ConnectionFactory, interval: Period) {
  implicit val connection = factory.getConnection
  val logger = LoggerFactory.getLogger(getClass)
  val closingHolder = new ClosingHolder
  var terminated = false

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  import materializer.executionContext

  def calculateStats() {
    CallOnQueue.deleteStalePendingCalls()
    val pendingQlCids = CallOnQueue.getPendingCallIds()
    logger.info(s"Removing pending calls for table call_on_queue : $pendingQlCids")
    CallOnQueue.deleteByCallid(pendingQlCids)

    CallData.deleteStalePendingCalls()
    val pendingCelCids = CallData.getPendingCallIds()
    logger.info(s"Removing pending calls for table call_data : $pendingCelCids")
    CallData.deleteByCallid(pendingCelCids)

    def unitTo[T](f: T => Unit)(t: T): T = {
      f(t)
      t
    }

    val callDataGenerator = new CallDataGenerator
    val celIdManager = new CelIdManager
    val celParsers = List(callDataGenerator, celIdManager)

    val callOnQueueGenerator = new CallOnQueueGenerator
    val agentPositionManager = new AgentPositionManager
    val statAgentManager = new StatAgentManager(interval)
    val statQueueManager = new StatQueueManager(interval, QueueLog.getLastId() + 1)
    val queueLogIdManager = new QueueLogIdManager
    val qlParsers = List(callOnQueueGenerator, agentPositionManager, statAgentManager, statQueueManager, queueLogIdManager)

    val processingGraph = GraphDSL.create() {
      implicit builder =>

        import GraphDSL.Implicits._

        val broadcaster = builder.add(Broadcast[CallEvent](2))

        val callData = Flow.fromFunction(unitTo(callDataGenerator.parseEvent))
        val celId = Flow.fromFunction(unitTo(celIdManager.parseEvent))

        val callOnQueue = Flow.fromFunction(unitTo(callOnQueueGenerator.parseEvent))
        val agentPosition = Flow.fromFunction(unitTo(agentPositionManager.parseEvent))
        val statAgent = Flow.fromFunction(unitTo(statAgentManager.parseEvent))
        val statQueue = Flow.fromFunction(unitTo(statQueueManager.parseEvent))
        val queueLogId = Flow.fromFunction(unitTo(queueLogIdManager.parseEvent))
        val qlogBroadcast = builder.add(Broadcast[QueueLog](4))

        val cel = broadcaster.collect { case e: Cel => e }
        val qlog = broadcaster.collect { case e: QueueLog => e }

        val mergeQlog = builder.add(ZipN[QueueLog](4))
        val headOfSeq = Flow.fromFunction((s: Seq[QueueLog]) => s.head)

        val mergeAll = builder.add(Merge[CallEvent](2))

        cel ~> callData ~> celId ~> mergeAll

        qlog ~> qlogBroadcast ~> callOnQueue   ~> mergeQlog ~> headOfSeq ~> queueLogId ~> mergeAll
                qlogBroadcast ~> agentPosition ~> mergeQlog
                qlogBroadcast ~> statAgent     ~> mergeQlog
                qlogBroadcast ~> statQueue     ~> mergeQlog

        FlowShape(broadcaster.in, mergeAll.out)
    }

    val eventIterator = new CallEventIterator(Cel.getLastId() + 1, QueueLog.getLastId() + 1, pendingCelCids, pendingQlCids, closingHolder)(factory)

    val eventSourceStream: EventSourceStream = new EventSourceStream(Cel.getLastId() + 1, QueueLog.getLastId() + 1, closingHolder)({
      val factory = new ConnectionFactory(appConf.getString("reporting.address"),
        appConf.getString("reporting.database.name"),
        appConf.getString("reporting.database.user"),
        appConf.getString("reporting.database.password"))
      factory.getConnection
    }, system, materializer)

    val done = Source.unfold(eventIterator) { it =>
      if (closingHolder.isClosed) {
        None
      } else {
        if (!it.hasNext) {
          it.searchNewData()
        }
        Some(it, it)
      }
    }
      .throttle(1, 100.millis, 1, ThrottleMode.shaping)
      .flatMapConcat { it => Source.fromIterator(() => it) }
      .via(processingGraph)
      .runWith(Sink.ignore)

    val callEventsGraph = eventSourceStream.runnableGraph().run()

    done.onComplete { _ =>
      Try((celParsers ++ qlParsers).foreach(_.saveState()))
      callEventsGraph.shutdown()
      system.terminate()
      terminated = true
    }
  }

  def terminate(): Unit = {
    val maxRetries = 10
    var nbRetries = 0

    logger.info("Asking call events processing to terminate...")
    closingHolder.close()

    nbRetries = 0
    while (!terminated && nbRetries < maxRetries) {
      logger.info("Waiting 1 second for current state to be saved...")
      Thread.sleep(1000)
      nbRetries += 1
    }
    if (!terminated) {
      logger.warn("Could not terminate safely, exiting !")
    }

  }
}
