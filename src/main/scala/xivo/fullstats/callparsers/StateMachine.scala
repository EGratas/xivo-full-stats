package xivo.fullstats.callparsers

import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.fullstats.model.{CallSummaryTable, CallSummary, CallEvent}

abstract class StateMachine[E <: CallEvent, R <: CallSummary](table: CallSummaryTable[R])(implicit c: Connection) {
  val logger = LoggerFactory.getLogger(getClass)
  var state: State[E, R] = getInitialState
  var hasFailed = false

  def getInitialState: State[E, R]
  def forceCloture(): Unit
  def processEvent(event: E): Unit = {
    if(hasFailed)
      logger.info(s"Skipping event $event because an exception was thrown previously")
    else {
      try {
        val newState = state.processEvent(event)
        logger.debug(s"$newState over $state")
        if(newState != state) {
          newState.setResult(table.insertOrUpdate(newState.getResult))
          state = newState
        }
      } catch {
        case e: Exception => logger.warn(s"An exception occured when processing this event : $event", e)
          logger.info("Retrieved information is : " + state.getResult)
          hasFailed = true
      }
    }
  }
  def getResult: Option[R] = if(hasFailed) None else Some(state.getResult)
  def isCallFinished: Boolean = state.isCallFinished
}
