package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{CallData, Cel}

class Ringing(callData: CallData, cel: Cel) extends CelState(callData, false) {
  var ringStartTime = cel.eventTime

  override def processCel(cel: Cel): CelState = {
    if(cel.eventType.equals("ANSWER") && List("AppDial","AppQueue").contains(cel.appName)) {
      val seconds = (cel.eventTime.toDate.getTime - ringStartTime.toDate.getTime)/1000
      result.ringDurationOnAnswer = Some(seconds.toInt)
      result.cidNum = Some(cel.cidNum)
      if(result.dstNum.isEmpty)
        result.dstNum = result.cidNum
      return new Answered(result, cel)
    } else if(cel.eventType.equals("HANGUP") && cel.appName.equals("AppDial")) {
      if (result.dstAgent.isEmpty)
        result.agentId = extractAgentId(cel.exten)
      result.cidNum = Some(cel.cidNum)
      return new Started(result, cel)
    } else if(cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(result, cel)
    }
    this
  }

  private def extractAgentId(exten: String): Option[Int] = {
    if (exten.length > 3 && exten.contains("id-"))
      Some(exten.substring(3).toInt)
    else
      None
  }
}