package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{AttachedData, CallData, CallDirection, Cel}

class Started(callData: CallData, x: Cel) extends CelState(callData, false) {

  override def processCel(cel: Cel): CelState = {
    if (cel.eventType.equals("ANSWER")) {
      if (result.dstNum.isEmpty)
        result.dstNum = Some(cel.cidNum)
      if(List("AppDial", "MeetMe").contains(cel.appName) ||
        (List("AppDial2").contains(cel.appName) && result.statAppName.contains("Originate")))
        return new Answered(result, cel)
    } else if (cel.eventType.equals("APP_START") && cel.appName.equals("MeetMe")) {
      return new Answered(result, cel)
    } else if (cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(result, cel)
    } else if (cel.eventType.equals("APP_START") && List("Dial","Queue").contains(cel.appName)) {
      if (result.dstNum.isEmpty)
        result.dstNum = Some(cel.cidNum)
      return new Ringing(result, cel)
    } else if (cel.eventType.equals("XIVO_OUTCALL")) {
      result.callDirection = CallDirection.Outgoing
    } else if (cel.eventType.equals("XIVO_INCALL")) {
      result.callDirection = CallDirection.Incoming
    } else if(cel.eventType.equals("OUTCALL_ACD")) {
      result.dstNum = Some(cel.cidNum)
      return new StartedAcd(result, cel)
    } else if (cel.eventType.equals("XIVO_CUID")) {
      val split = cel.appData.split(',')
      if (split.length == 2) {
        result.addAttachedData(AttachedData("XIVO_CUID", split(1)))
      }
    }
    this
  }

}

