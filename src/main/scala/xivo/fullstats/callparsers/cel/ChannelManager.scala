package xivo.fullstats.callparsers.cel

import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.fullstats.model.Cel

import scala.collection.mutable.ListBuffer

class ChannelManager(val callDataId: Int)(implicit c: Connection) {
  val logger = LoggerFactory.getLogger(getClass)
  var channels = ListBuffer[ChannelMachine]()

  def processEvent(cel: Cel): Unit = {
    (cel.eventType, cel.appName) match {
      case ("APP_START", "Dial") => extractInterfaces(cel.appData).foreach(interface =>
        channels.append(new ChannelMachine(interface, callDataId, cel.eventTime)))
      case ("BRIDGE_ENTER", "Queue") if cel.context == "group" => extractPeer(cel.peer).foreach(interface =>
        channels.append(new ChannelMachine(interface, callDataId, cel.eventTime, true)))
      case ("BRIDGE_UPDATE", "Dial") => extractInterfaces(cel.appData).foreach(interface =>
        channels.append(new ChannelMachine(interface, callDataId, cel.eventTime, true)))
      case ("LINKEDID_END", _) => channels.foreach(_.processEvent(cel))
      case _ => channels.find(_.isEventForMe(cel)).foreach(machine => {
        machine.processEvent(cel)
        if(machine.isChannelHangedUp) channels -= machine
      })
    }
  }

  def extractInterfaces(appData: String): List[String] = {
    if(appData.nonEmpty) appData.split(",").headOption match {
      case None => List()
      case Some(interfacesStr) => interfacesStr.split("&").map(
        s => s.split("/").take(2).mkString("/")
      ).toList
    }
    else List()
  }

  private def extractPeer(peer: String): Option[String] = {
    if(peer.nonEmpty) peer.split("-").headOption
    else None
  }

  def forceCloture(): Unit = channels.foreach(_.forceCloture())
}
