package xivo.fullstats

import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

object Main {

  val logger = LoggerFactory.getLogger(getClass)
  val appConf = ConfigFactory.load()
  var xivoStat: XivoStat = null

  def main(args: Array[String]): Unit = {
    try {
      val period = TimeUtils.parseInterval(appConf.getString("reporting.interval"))
      logger.info(s"starting with period $period")
      val factory = new ConnectionFactory(appConf.getString("reporting.address"),
        appConf.getString("reporting.database.name"),
        appConf.getString("reporting.database.user"),
        appConf.getString("reporting.database.password"))
      xivoStat = new XivoStat(factory, period)
      sys.addShutdownHook(shutdownHook)
      xivoStat.calculateStats()
    } catch {
      case e: Exception => logger.error("An error occurred during statistics call_on_queue or cel processing", e)
    }
  }

  def shutdownHook(): Unit = {
    logger.info("Shutdown signal received, exiting...")
    xivoStat.terminate()
  }

}