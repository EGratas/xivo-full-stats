package xivo.fullstats.streams

import java.sql.Connection

import akka.Done
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge, Partition, Sink}
import akka.stream.{ActorAttributes, Graph, SinkShape}
import xivo.fullstats.model.{CallChannel, CallEvent, CallThreadEvent, QueueCall}
import xivo.fullstats.streams.callChannel.{CallChannelGraph, CallChannelRepository}
import xivo.fullstats.streams.conversation.{ConversationGraph, ConversationRepository}
import xivo.fullstats.streams.queueCall.{QueueCallGraph, QueueCallRepository}
import xivo.fullstats.streams.transfer.{TransferGraph, TransferRepository}

import scala.concurrent.Future

class CallEventsProcessingGraph(implicit val connection: Connection)
  extends CallEventsGraph[SinkShape[CallEvent], Future[Done]] {

  implicit val callChannelRepository: CallChannelRepository = new CallChannelRepository
  implicit val queueCallRepository: QueueCallRepository = new QueueCallRepository
  implicit val conversationRepository: ConversationRepository = new ConversationRepository
  implicit val transferRepository: TransferRepository = new TransferRepository

  val callChannel = new CallChannelGraph
  val queueCall = new QueueCallGraph
  val conversation = new ConversationGraph
  val transferCall = new TransferGraph

  val dbActions = new DatabaseActions

  val outsideSink: Sink[CallThreadEvent, Future[Done]] = Sink.ignore

  val graph: Graph[SinkShape[CallEvent], Future[Done]] = GraphDSL.create(outsideSink) {
    implicit builder => sink =>

      import GraphDSL.Implicits._

      val COMPLETED = 1
      val NOTCOMPLETED = 0

      def callChannelDeleted(callChannel: CallChannel): Int = if (callChannel.deleted) COMPLETED else NOTCOMPLETED
      def queueCalDeleted(queueCall: QueueCall): Int = if (queueCall.deleted) COMPLETED else NOTCOMPLETED

      val broadcaster = builder.add(Broadcast[CallEvent](4))
      val merge = builder.add(Merge[CallThreadEvent](6))
      val partitionCallChannels = builder.add(Partition[CallChannel](2, callChannelDeleted))
      val partitionQueueCalls = builder.add(Partition[QueueCall](2, queueCalDeleted))

      val callChannelFlow = Flow.fromGraph(callChannel.graph)
      val queueCallFlow = Flow.fromGraph(queueCall.graph)
      val conversationFlow = Flow.fromGraph(conversation.graph)
      val transferFlow = Flow.fromGraph(transferCall.graph)

      broadcaster ~> callChannelFlow  ~> partitionCallChannels.in
      broadcaster ~> queueCallFlow    ~> partitionQueueCalls.in
      broadcaster ~> conversationFlow                                  ~> dbActions.updateInDatabase                                  ~> merge
      broadcaster ~> transferFlow                                      ~> dbActions.updateInDatabase                                  ~> merge

      partitionCallChannels.out(NOTCOMPLETED)                          ~> dbActions.updateInDatabase  ~> dbActions.updateInRepository ~> merge
      partitionCallChannels.out(COMPLETED)                             ~> dbActions.updateInDatabase                                  ~> merge

      partitionQueueCalls.out(NOTCOMPLETED)                            ~> dbActions.updateInDatabase  ~> dbActions.updateInRepository ~> merge
      partitionQueueCalls.out(COMPLETED)                               ~> dbActions.updateInDatabase                                  ~> merge

      merge ~> sink

      SinkShape(broadcaster.in)
  }.withAttributes(ActorAttributes.supervisionStrategy(decider))
}
