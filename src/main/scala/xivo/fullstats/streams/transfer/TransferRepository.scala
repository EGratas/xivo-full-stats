package xivo.fullstats.streams.transfer

import xivo.fullstats.model.TransferLinks

import scala.collection.mutable

class TransferRepository {
    val repository: mutable.Map[String, List[TransferLinks]] = mutable.Map()

    def addToMap(key: String, value: List[TransferLinks]) = {
      repository += (key -> value)
      value
    }

    def getFromMap(key: String): Option[List[TransferLinks]] = {
      repository.get(key)
    }

    def deleteFromMap(key: String): Option[List[TransferLinks]] = {
      repository.remove(key)
    }
}
