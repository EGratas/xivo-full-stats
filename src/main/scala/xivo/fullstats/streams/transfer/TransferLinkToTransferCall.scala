package xivo.fullstats.streams.transfer

import java.sql.Connection

import akka.stream.scaladsl.{Flow, Sink}
import akka.{Done, NotUsed}
import xivo.fullstats.model.{CallChannel, Cel, TransferCall, TransferLinks}

import scala.concurrent.Future

class TransferLinkToTransferCall(implicit connection: Connection, transferRepository: TransferRepository) {

  val flow: Flow[List[TransferLinks], TransferCall, NotUsed] = Flow[List[TransferLinks]]
    .collect{ case t => t.filter(p => p.transferDecisionId.isDefined) }
    .alsoTo(deleteFromMap)
    .map(createTransferCall)
    .mapConcat(identity)

  private def deleteFromMap: Sink[List[TransferLinks], Future[Done]] = Sink.foreach{ t =>
    t.foreach(e => transferRepository.deleteFromMap(e.linkedId))
  }

  private def createTransferCall(transferLinks: List[TransferLinks]) = {
    transferLinks.flatMap { t =>
      for {
        initId <- CallChannel.getIdByUniqueId(t.linkedId)
        endId  <- CallChannel.getIdByUniqueId(getEnd(t))
        holdId <- tryCallChannelId(t.onHold)
      } yield TransferCall(
        id = None,
        init = initId,
        end = endId,
        hold = holdId,
        talk = tryCallChannelId(getTalk(t)),
        transferTime = t.transferTime.get
      )
    }
  }

  private def getEnd(transferLinks: TransferLinks): String = transferLinks.collectedUniqueIds.max

  private def getTalk(transferLinks: TransferLinks): Option[String] = {
    val onTalk = Cel.getLinkedIdFromUniqueId(getEnd(transferLinks))
    if (onTalk.contains(transferLinks.linkedId)) None
    else onTalk
  }

  private def tryCallChannelId(asteriskId: Option[String]): Option[Long] = asteriskId.flatMap(CallChannel.getIdByUniqueId)
}
