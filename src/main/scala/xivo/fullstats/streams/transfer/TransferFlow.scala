package xivo.fullstats.streams.transfer

import java.sql.Connection

import akka.{Done, NotUsed}
import akka.stream.scaladsl.{Flow, Sink}
import org.slf4j.LoggerFactory
import xivo.fullstats.model.{Cel, TransferLinks, TypedCelEvent}

import scala.concurrent.Future

trait TransferFlow[T <: TypedCelEvent] {
  val logger = LoggerFactory.getLogger(getClass)

  val flow: Flow[T, _, NotUsed]
  var savedCel: Cel = _

  def log(c: T) = {
    logger.info(s"Processing event $c")
    c
  }

  def keepCel(c: T) = {
    savedCel = c.cel
    c
  }

  def getTransferLinks(implicit repository: TransferRepository) = Flow[T]
    .map(c => repository.getFromMap(c.cel.linkedId))

  def filterByContext(contextList: List[String]) = Flow[T].collect{
    case c if !contextList.contains(c.cel.context) => c
  }

  def createNewTransferLink(implicit c: Connection): List[TransferLinks] = {
    List(TransferLinks(savedCel.linkedId, List(savedCel.uniqueId), None, None, None))
  }

  def updateExistingTransferLink(transferLinks: List[TransferLinks])(implicit c: Connection): List[TransferLinks] = {
    transferLinks.map{
      case t if t.stopCollecting => t
      case t => addLinkRelatedUniqueIds(t)
    }
  }

  def addLinkRelatedUniqueIds(transferLinks: TransferLinks)(implicit c: Connection) = {
    transferLinks.copy(collectedUniqueIds = (savedCel.uniqueId :: transferLinks.collectedUniqueIds).distinct)
  }

  def deleteFromMap(implicit repository: TransferRepository): Sink[Option[List[TransferLinks]], Future[Done]] = Sink.foreach{
    case Some(t) => repository.deleteFromMap(t.head.linkedId)
    case None => repository.deleteFromMap(savedCel.linkedId)
  }
}
