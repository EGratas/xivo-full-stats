package xivo.fullstats.streams.callChannel

import xivo.fullstats.model.CallChannel

import scala.collection.mutable

class CallChannelRepository {

  var repository: mutable.HashMap[String, CallChannel] = mutable.HashMap()

  def addCallToMap(key: String, value: CallChannel): CallChannel = {
    repository += (key -> value)
    value
  }

  def getCallFromMap(key: String): Option[CallChannel] = {
    repository.get(key)
  }

  def deleteCallFromMap(key: String): Option[CallChannel] = {
    repository.remove(key)
  }
}
