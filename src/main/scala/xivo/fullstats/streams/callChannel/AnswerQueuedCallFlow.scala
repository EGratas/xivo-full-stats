package xivo.fullstats.streams.callChannel

import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import org.joda.time.DateTime
import xivo.fullstats.model.CallChannel
import xivo.fullstats.streams.callChannel.cel.Answer.LinkedCallChannels

final class AnswerQueuedCallFlow(getRingDuration: (CallChannel,Option[DateTime]) => Option[Long])(implicit repository: CallChannelRepository)
  extends GraphStage[FlowShape[LinkedCallChannels, CallChannel]] {
  val in: Inlet[LinkedCallChannels] = Inlet[LinkedCallChannels]("AnswerQueuedCallFlow.in")
  val out: Outlet[CallChannel] = Outlet[CallChannel]("AnswerQueuedCallFlow.out")

  override val shape: FlowShape[LinkedCallChannels, CallChannel] = FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {

      setHandlers(in, out, new InHandler with OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }

        override def onPush(): Unit = {
          val linkedCallChannels: LinkedCallChannels = grab(in)
          val currentCallChannel: CallChannel = linkedCallChannels.acdCallee
          val queuedCallChannel: Option[CallChannel] = linkedCallChannels.queuedCaller.map(_.withAnswer(currentCallChannel.answerTime, getRingDuration))

          emitMultiple(out, List(Some(currentCallChannel), queuedCallChannel).flatten.distinct)
        }
      })
    }
}
