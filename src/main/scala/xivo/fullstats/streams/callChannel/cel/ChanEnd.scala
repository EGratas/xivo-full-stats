package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class ChanEnd(implicit repository: CallChannelRepository) extends CallChannelFlow[ChanEndCel] {

  val allowedContext = List("user", "queue")
  val allowedAppName = List("AppDial", "AppQueue")
  val allowedChannel = List("DAHDI")
  val allowedExtension = List("*")

  val getByAllowed = Flow[ChanEndCel]
    .collect {
      case c
        if allowedChannel.exists(c.cel.chanName.startsWith)
          || allowedExtension.exists(c.cel.exten.startsWith)
          || allowedContext.contains(c.cel.context)
          || allowedAppName.contains(c.cel.appName) => c
    }

  val flow: Flow[ChanEndCel, CallChannel, NotUsed] = Flow[ChanEndCel]
    .map(keepCel)
    .via(getByAllowed)
    .map(log)
    .via(deleteCallChannelFromRepository)
}
