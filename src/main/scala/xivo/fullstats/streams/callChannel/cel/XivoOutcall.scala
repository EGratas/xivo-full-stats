package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class XivoOutcall(implicit repository: CallChannelRepository) extends CallChannelFlow[XivoOutcallCel] {

  val flow: Flow[XivoOutcallCel, CallChannel, NotUsed] = Flow[XivoOutcallCel]
    .map(keepCel)
    .map(log)
    .via(getByUniqueIdOrLinkedId)
    .map(setCallType)
    .map(updateCallChannelDirection)
    .map(finishAndSave)

  private def updateCallChannelDirection(callChannel: CallChannel): CallChannel = {
    callChannel.copy(
      emitted = true
    )
  }

  private def setCallType(callChannel: CallChannel): CallChannel = {
    findOngoingWithSameUserId(callChannel).fold(callChannel)(_ => callChannel.copy(callType = CallType.Consultation))
  }
}
