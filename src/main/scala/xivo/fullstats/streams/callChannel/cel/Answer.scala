package xivo.fullstats.streams.callChannel.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.joda.time.DateTime
import xivo.fullstats.agent.AgentPositionProxy
import xivo.fullstats.model.{AgentFeatures, _}
import xivo.fullstats.streams.callChannel.CallChannelFlow._
import xivo.fullstats.streams.callChannel.cel.Answer.LinkedCallChannels
import xivo.fullstats.streams.callChannel.{AnswerQueuedCallFlow, CallChannelFlow, CallChannelRepository}

object Answer {
  case class LinkedCallChannels(queuedCaller: Option[CallChannel], acdCallee: CallChannel)
}

class Answer(implicit repository: CallChannelRepository, connection: Connection) extends CallChannelFlow[AnswerCel] {

  val emitCurrentAndQueuedCallChannelIfExists = new AnswerQueuedCallFlow(getRingDuration)

  val flow: Flow[AnswerCel, CallChannel, NotUsed] = Flow[AnswerCel]
    .map(keepCel)
    .via(getByAppName(List("AppDial", "AppDial2", "AppQueue", "Answer", "Dial", "Pickup", "MeetMe")))
    .via(filterByContext(List("agentcallback")))
    .map(log)
    .via(getByUniqueIdOrLinkedId)
    .map(processAgentId)
    .map(processAnswer)
    .via(emitCurrentAndQueuedCallChannelIfExists)
    .map(finishAndSave)

  def processAgentId(callChannel: CallChannel): CallChannel = {
    def addMaybeAgentId() = {
      val maybeAgentId = getAgentId(savedCel.cidNum, savedCel.eventTime)

      callChannel.callType match {
        case CallType.Consultation => callChannel.withAgent(maybeAgentId)
        case CallType.Acd => callChannel.withAgent(maybeAgentId)
        case _ => callChannel
      }
    }

    callChannel.agentId match {
      case Some(_) => callChannel
      case _ => addMaybeAgentId()
    }
  }

  private def processAnswer(callChannel: CallChannel): LinkedCallChannels = {
    (savedCel.context, savedCel.chanName) match {
      case _ if callChannel.endTime.isDefined => getLinkedCallChannels(callChannel)
      case (context, channel) if channel.contains("Local/") && context == "user" => LinkedCallChannels(None, callChannel.withAnswer(Option(savedCel.eventTime), getRingDuration))
      case (_, channel) if channel.contains("Local/") => LinkedCallChannels(None, callChannel)
      case (context, _) if context == "xivo-pickup" => getLinkedCallChannels(callChannel)
      case _ => getLinkedCallChannels(callChannel.withAnswer(Option(savedCel.eventTime), getRingDuration))
    }
  }

  private def getLinkedCallChannels(callChannel: CallChannel) = {
    val linkedQueuedOrConsultationCall: Option[CallChannel] = findLinkedQueuedOrConsultationCallInRepo(savedCel.linkedId)
    LinkedCallChannels(linkedQueuedOrConsultationCall, callChannel)
  }

  private def getAgentId(agentNumber: String, eventTime: DateTime): Option[Long] = {
    AgentPositionProxy.agentForNumAndTime(agentNumber, eventTime).flatMap(number =>
      AgentFeatures.getAgentByNumber(number).map(_.id.toLong)
    )
  }

  private def findLinkedQueuedOrConsultationCallInRepo(linkedId: String): Option[CallChannel] = {
    repository.getCallFromMap(linkedId)
      .flatMap{ notAnsweredCall =>
        matchCallType(CallType.Queued)(notAnsweredCall).orElse(matchCallType(CallType.Consultation)(notAnsweredCall))}
      .flatMap(isOngoing)
  }
}
