package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class LinkedIdEnd(implicit repository: CallChannelRepository) extends CallChannelFlow[LinkedIdEndCel] {

  val allowedExtension = List("*")
  val allowedAppName = List("AppDial")
  val disallowContext = List("xuc_attended_xfer_wait")

  val filterPickupCall = Flow[LinkedIdEndCel]
    .collect {
      case c if !(allowedAppName.contains(c.cel.appName) && allowedExtension.exists(c.cel.cidDnid.startsWith)) => c
    }

  val flow: Flow[LinkedIdEndCel, CallChannel, NotUsed] = Flow[LinkedIdEndCel]
    .map(log)
    .via(filterPickupCall)
    .via(deleteLinkedCallChannelFromRepository)
}
