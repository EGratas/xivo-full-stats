package xivo.fullstats.streams.source

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Concat, Source}
import akka.stream.{ActorAttributes, ActorMaterializer, Supervision, ThrottleMode}
import org.joda.time.DateTime
import org.slf4j.{Logger, LoggerFactory}
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model.{CallChannel, Cel}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

class CelSource(startCelId: Int, closingHolder: ClosingHolder, pendingStartTime : Option[DateTime], connectionForPendingCels: Connection)(implicit val c: Connection, materializer: ActorMaterializer) {
  import materializer.executionContext
  final val BATCH_SIZE = 50000
  final val CEL_NEW_DATA_INITIAL_DELAY = 2000.milliseconds
  final val CEL_NEW_DATA_DELAY_AFTER_QUEUE_LOGS = 500.milliseconds

  val logger: Logger = LoggerFactory.getLogger(getClass)

  val decider: Supervision.Decider = {
    case e: Exception =>
      logger.error(s"Exception occurred: $e - ${e.printStackTrace()}")
      Supervision.Resume
  }

  CallChannel.setStalePendingCalls
  private val pendingCelUniqueIds: List[String] = getPendingCelUniqueIds(pendingStartTime)
  logger.info(s"Removing pending call channels for table xc_call_channel : $pendingCelUniqueIds")
  CallChannel.deleteById(pendingCelUniqueIds)

  val pendingCelsSource: Source[Cel, Unit] = {
    Cel.getPendingCelsByUniqueIds(pendingCelUniqueIds, startCelId, connectionForPendingCels, BATCH_SIZE)
      .mapMaterializedValue(_.onComplete {
        case Success(s) => logger.info(s"Finished retrieving $s pending cels")
          connectionForPendingCels.close()
        case Failure(f) => logger.error(s"Error while retrieving the pending cels: $f")
          connectionForPendingCels.close()
      })
  }

  val celsSource: Source[Cel, NotUsed] =
    Source.unfold(startCelId)(fetchCels).initialDelay(CEL_NEW_DATA_INITIAL_DELAY)
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
      .throttle(1, CEL_NEW_DATA_DELAY_AFTER_QUEUE_LOGS, 1, ThrottleMode.shaping)
      .flatMapConcat { cels => Source.fromIterator(() => cels.iterator) }

  val source: Source[Cel, NotUsed] = Source.combine(pendingCelsSource, celsSource)(Concat(_))

  private def getPendingCelUniqueIds: Option[DateTime] => List[String] = {
    case Some(t) => CallChannel.getAllPendingUniqueIds(CallChannel.getPendingUniqueIds(t))
    case None => List()
  }

  private def fetchCels: Int => Option[(Int, List[Cel])] = { celId: Int =>
    if (closingHolder.isClosed) {
      None
    }
    else {
      val res: List[Cel] = Cel.getCels(celId, celId + BATCH_SIZE)
      val lastId = if (res.isEmpty) celId else res.last.id + 1
      Some(lastId, res)
    }
  }
}
