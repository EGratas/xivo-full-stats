package xivo.fullstats.streams.queueCall.queuelog

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.joda.time.Seconds
import xivo.fullstats.model.{CompleteQl, QueueCall}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

class Complete(implicit repository: QueueCallRepository, connection: Connection)
    extends QueueCallFlow[CompleteQl] {

  val flow: Flow[CompleteQl, QueueCall, NotUsed] = Flow[CompleteQl]
    .map(keepQl)
    .map(log)
    .via(tryGetQueueCall)
    .map(updateEndTime)
    .map(updateTerminationType)
    .map(updateRingDuration)
    .map(finishAndDelete)

  private def updateRingDuration(queueCall: QueueCall): QueueCall = {
    queueCall.answerTime match {
      case Some(_) => queueCall
      case None =>
        queueCall.copy(ringDuration =
          Some(
            Seconds.secondsBetween(queueCall.startTime, savedQl.time).getSeconds
          )
        )
    }
  }

  private def finishAndDelete(queueCall: QueueCall) = {
    repository.deleteCallFromMap(queueCall.uniqueId)
    queueCall.asDeleted
  }
}
