package xivo.fullstats.streams.conversation

import java.sql.Connection

import akka.stream.scaladsl.{Flow, Sink}
import akka.{Done, NotUsed}
import org.slf4j.LoggerFactory
import xivo.fullstats.model.{Cel, ConversationLinks, TypedCelEvent}

import scala.concurrent.Future

trait ConversationFlow[T <: TypedCelEvent] {
  val logger = LoggerFactory.getLogger(getClass)

  val flow: Flow[T, _, NotUsed]
  var savedCel: Cel = _

  def log(c: T) = {
    logger.info(s"Processing event $c")
    c
  }

  def keepCel(c: T) = {
    savedCel = c.cel
    c
  }

  def getConversationLinks(implicit repository: ConversationRepository) = Flow[T]
    .map(c => repository.getFromMap(c.cel.linkedId))

  def filterByContext(contextList: List[String]) = Flow[T].collect{
    case c if !contextList.contains(c.cel.context) => c
  }

  def filterByApp(appNameList: List[String]) = Flow[T].collect{
    case c if !appNameList.contains(c.cel.appName) => c
  }

  def addLinkRelatedUniqueIds(conversationLinks: ConversationLinks)(implicit c: Connection) = {
    conversationLinks.copy(relatedUniqueIds = (savedCel.uniqueId :: conversationLinks.relatedUniqueIds).distinct)
  }

  def createNewConversationLink(implicit c: Connection): ConversationLinks = {
    ConversationLinks(savedCel.linkedId, List(savedCel.uniqueId))
  }

  def updateExistingConversationLink(conversationLinks: ConversationLinks)(implicit c: Connection): ConversationLinks = {
    addLinkRelatedUniqueIds(conversationLinks)
  }

  def deleteFromMap(implicit repository: ConversationRepository): Sink[ConversationLinks, Future[Done]] = Sink.foreach{c =>
    repository.deleteFromMap(c.linkedId)
  }
}
