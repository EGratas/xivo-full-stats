package xivo.fullstats.streams.conversation.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{ConversationLinks, LinkedIdEndCel}
import xivo.fullstats.streams.conversation.{ConversationFlow, ConversationRepository}

class LinkedIdEnd(implicit connection: Connection, conversationRepository: ConversationRepository) extends ConversationFlow[LinkedIdEndCel] {

  val flow: Flow[LinkedIdEndCel, ConversationLinks, NotUsed] = Flow[LinkedIdEndCel]
    .map(log)
    .via(getConversationLinks)
    .collect{ case Some(c) => c }
    .alsoTo(deleteFromMap)
}
