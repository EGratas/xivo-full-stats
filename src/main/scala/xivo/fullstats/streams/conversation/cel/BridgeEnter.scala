package xivo.fullstats.streams.conversation.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{BridgeEnterCel, ConversationLinks}
import xivo.fullstats.streams.conversation.{ConversationFlow, ConversationRepository}

class BridgeEnter(implicit connection: Connection, conversationRepository: ConversationRepository) extends ConversationFlow[BridgeEnterCel] {

  val flow: Flow[BridgeEnterCel, ConversationLinks, NotUsed] = Flow[BridgeEnterCel]
    .via(filterByContext(List("agentcallback", "xuc_attended_xfer_wait", "xuc_attended_xfer_end")))
    .map(keepCel)
    .map(log)
    .via(getConversationLinks)
    .map(processConversation)

  private def processConversation(maybeConversationLinks: Option[ConversationLinks]) = {
    maybeConversationLinks match {
      case Some(conversationLinks) => updateExistingConversationLink(conversationLinks)
      case None => createNewConversationLink
    }
  }
}
