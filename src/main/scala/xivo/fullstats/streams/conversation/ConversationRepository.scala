package xivo.fullstats.streams.conversation

import xivo.fullstats.model.ConversationLinks

import scala.collection.mutable

class ConversationRepository {

  val repository: mutable.Map[String, ConversationLinks] = mutable.Map()

  def addToMap(key: String, value: ConversationLinks): ConversationLinks = {
    repository += (key -> value)
    value
  }

  def getFromMap(key: String): Option[ConversationLinks] = {
    repository.get(key)
  }

  def deleteFromMap(key: String): Option[ConversationLinks] = {
    repository.remove(key)
  }
}
