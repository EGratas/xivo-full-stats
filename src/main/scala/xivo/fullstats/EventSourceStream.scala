package xivo.fullstats

import java.sql.Connection

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{GraphDSL, Keep, MergePreferred, RunnableGraph, Source}
import org.joda.time.DateTime
import xivo.fullstats.Main.appConf
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model.{CallChannel, CallEvent, QueueCall}
import xivo.fullstats.streams.CallEventsProcessingGraph
import xivo.fullstats.streams.common.PeriodicCleanPendingCalls
import xivo.fullstats.streams.source.{CelSource, QueueLogSource}

class EventSourceStream(startCelId: Int, startQueueLogId: Int, closingHolder: ClosingHolder)(implicit connection: Connection, system: ActorSystem, materializer: ActorMaterializer) {
  implicit def ordering: Ordering[Option[DateTime]] = new Ordering[Option[DateTime]] {
    implicit def startTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)
    override def compare(x: Option[DateTime], y: Option[DateTime]): Int = {
      (x, y) match {
        case (Some(qt), Some(cT)) => implicitly[Ordering[DateTime]].compare(qt, cT)
        case (None, Some(_)) => 1
        case (Some(_), None) => -1
        case (None, None) => -1
      }
    }
  }

  val pendingStartTime: Option[DateTime] = List(QueueCall.getPendingStartTime(), CallChannel.getPendingStartTime()).min

  private val queueLog = new QueueLogSource(startQueueLogId, closingHolder, pendingStartTime, getNewDatabaseConnection)
  private val cel = new CelSource(startCelId, closingHolder, pendingStartTime, getNewDatabaseConnection)

  private val callEventsProcessingGraph = new CallEventsProcessingGraph

  system.actorOf(PeriodicCleanPendingCalls.props(callEventsProcessingGraph.callChannelRepository, callEventsProcessingGraph.queueCallRepository,
    callEventsProcessingGraph.conversationRepository, callEventsProcessingGraph.transferRepository, system.scheduler))

  val callEventsSource: Graph[SourceShape[CallEvent], NotUsed] = GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val mergePreferred = builder.add(MergePreferred[CallEvent](1))

      cel.source      ~> mergePreferred.in(0)
      queueLog.source ~> mergePreferred.preferred

      SourceShape(mergePreferred.out)
  }

  def runnableGraph(): RunnableGraph[UniqueKillSwitch] = Source.fromGraph(callEventsSource)
    .viaMat(KillSwitches.single)(Keep.right)
    .toMat(callEventsProcessingGraph.graph)(Keep.left)

  def getNewDatabaseConnection: Connection = {
    val factory = new ConnectionFactory(appConf.getString("reporting.address"),
      appConf.getString("reporting.database.name"),
      appConf.getString("reporting.database.user"),
      appConf.getString("reporting.database.password"))
    val connection = factory.getConnection
    connection.setAutoCommit(false)
    connection
  }
}
