package xivo.fullstats.model

import java.sql.Connection

import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.streams.queueCall.QueueCallRepository

trait CallThreadEvent {
  val id: Option[Long]

  def insertOrUpdate(e: CallThreadEvent)(implicit connection:Connection): CallThreadEvent = {
    e match {
      case e: CallChannel => CallChannel.insertOrUpdate(e)
      case e: Conversation => Conversation.insertOrUpdate(e)
      case e: QueueCall => QueueCall.insertOrUpdate(e)
      case e: TransferCall => TransferCall.insertOrUpdate(e)
    }
  }

  def addToRepository(e: CallThreadEvent)(implicit callChannelRepository: CallChannelRepository,
                      queueCallRepository: QueueCallRepository): CallThreadEvent = {
    e match {
      case e: CallChannel => callChannelRepository.addCallToMap(e.uniqueId, e)
      case e: Conversation => e
      case e: QueueCall => queueCallRepository.addCallToMap(e.uniqueId, e)
    }
  }
}
