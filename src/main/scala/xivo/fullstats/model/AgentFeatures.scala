package xivo.fullstats.model

import java.sql.Connection
import anorm.SqlParser._
import anorm._

case class AgentFeatures(id:Int, number: String, firstName: String, lastName: String)

object AgentFeatures {

  val simple = get[Int]("id") ~
    get[String]("number") ~
    get[String]("firstname") ~
    get[String]("lastname") map {
    case id ~ number ~ firstname ~ lastname =>
      AgentFeatures(id, number, firstname, lastname)
  }

  def getAgentById(agentId: Option[Int])(implicit c: Connection): Option[AgentFeatures] =
    SQL("SELECT id, number, firstname, lastname FROM agentfeatures WHERE id = {agentId} LIMIT 1")
      .on('agentId -> agentId).as(simple *).headOption

  def getAgentByNumber(agentNumber: String)(implicit c: Connection): Option[AgentFeatures] =
    SQL("SELECT id, number, firstname, lastname FROM agentfeatures WHERE number = {agentNumber} LIMIT 1")
      .on('agentNumber -> agentNumber).as(simple *).headOption
}