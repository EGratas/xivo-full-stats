package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import akka.stream.Materializer
import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

sealed trait TypedCelEvent {
  val cel: Cel
}
case class ChanStartCel(cel: Cel) extends TypedCelEvent
case class XivoIncallCel(cel: Cel) extends TypedCelEvent
case class XivoOutcallCel(cel: Cel) extends TypedCelEvent
case class HangupCel(cel: Cel) extends TypedCelEvent
case class LinkedIdEndCel(cel: Cel) extends TypedCelEvent
case class AnswerCel(cel: Cel) extends TypedCelEvent
case class ChanEndCel(cel: Cel) extends TypedCelEvent
case class AppStartCel(cel: Cel) extends TypedCelEvent
case class BridgeEnterCel(cel: Cel) extends TypedCelEvent
case class BridgeExitCel(cel: Cel) extends TypedCelEvent
case class AttendedTransferCel(cel: Cel) extends TypedCelEvent
case class BlindTransferCel(cel: Cel) extends TypedCelEvent
case class XivoAttendedTransferEndCel(cel: Cel) extends TypedCelEvent
case class XivoAttendedTransferWaitCel(cel: Cel) extends TypedCelEvent

trait CelInterface {

  def linkedIdFromUniqueId(uniqueid: String)(implicit c: Connection): Option[String]
  def maximumId(implicit c: Connection): Option[Int]
  def eventTimeById(id: Int)(implicit c: Connection): Option[DateTime]
  def minimumId(implicit c: Connection): Option[Int]
}

case class Cel(
  override val id: Int,
  var eventType: String,
  var eventTime: DateTime,
  var userDefType: String,
  var cidName: String,
  var cidNum: String,
  var cidAni: String,
  var cidRdnis: String,
  var cidDnid: String,
  var exten: String,
  var context: String,
  var chanName: String,
  var appName: String,
  var appData: String,
  var amaFlags: Int,
  var accountCode: String,
  var peerAccount: String,
  var uniqueId: String,
  var linkedId: String,
  var userField: String,
  var peer: String,
  var extra: String,
  var callLogId: Option[Int]) extends CallEvent(id, linkedId, eventTime)

object CelAppName {
  sealed trait AppName
  case object DialApp extends AppName
  case object QueueApp extends AppName
}

object Cel extends LastIdTable with CelInterface {
  override val lastIdTable: String = "last_cel_id"

  val cel = get[Int]("id") ~
    get[String]("eventtype") ~
    get[DateTime]("eventtime") ~
    get[String]("userdeftype") ~
    get[String]("cid_name") ~
    get[String]("cid_num") ~
    get[String]("cid_ani") ~
    get[String]("cid_rdnis") ~
    get[String]("cid_dnid") ~
    get[String]("exten") ~
    get[String]("context") ~
    get[String]("channame") ~
    get[String]("appname") ~
    get[String]("appdata") ~
    get[Int]("amaflags") ~
    get[String]("accountcode") ~
    get[String]("peeraccount") ~
    get[String]("uniqueid") ~
    get[String]("linkedid") ~
    get[String]("userfield") ~
    get[String]("peer") ~
    get[String]("extra") map {
    case id ~ eventtype ~ eventtime ~ userdeftype ~ cid_name ~ cid_num ~  cid_ani ~ cid_rdnis ~ cid_dnid ~ exten ~
      context ~ channame ~ appname ~ appdate ~ amaflags ~ accountcode ~ peeraccount ~ uniqueid ~ linkedid ~ userfield ~ peer ~ extra =>
      Cel(id, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame,
        appname, appdate, amaflags, accountcode, peeraccount, uniqueid, linkedid, userfield, peer, extra, None)
  }

  def linkedIdFromUniqueId(uniqueid: String)(implicit c: Connection): Option[String] = SQL("SELECT linkedid FROM cel WHERE uniqueid = {uniqueid} LIMIT 1")
    .on('uniqueid -> uniqueid).as(get[String]("linkedid").*).headOption

  def maximumId(implicit c: Connection): Option[Int] = SQL("SELECT max(id) AS max FROM cel").as(get[Option[Int]]("max") *).head

  def eventTimeById(id: Int)(implicit c: Connection): Option[DateTime] = SQL("SELECT eventtime FROM cel WHERE id = {id}").on('id -> id)
    .as(get[Date]("eventtime").*).map(new DateTime(_)).headOption

  def minimumId(implicit c: Connection): Option[Int] = SQL("SELECT min(id) AS min FROM cel").as(get[Option[Int]]("min") *).head


  def getUniqueIdFromLinkedId(linkedid: String, eventType: String, appName: String, context: String)(implicit c: Connection): Option[String] = {
    SQL("SELECT uniqueid FROM cel WHERE linkedid={linkedid} AND eventtype={eventtype} AND appname={appname} AND context={context} LIMIT 1")
      .on('linkedid -> linkedid, 'eventtype -> eventType, 'appname -> appName, 'context -> context)
      .as(get[String]("uniqueid").*)
      .headOption
  }

  def getUniqueIdFromLinkedId(linkedid: String, eventType: String)(implicit c: Connection): Option[String] = {
    SQL("SELECT uniqueid FROM cel WHERE linkedid={linkedid} AND eventtype={eventtype} LIMIT 1")
      .on('linkedid -> linkedid, 'eventtype -> eventType)
      .as(get[String]("uniqueid").*)
      .headOption
  }

  def getLinkedIdFromUniqueId(uniqueid: String, eventType: String, appName: String, context: String)(implicit c: Connection): Option[String] = {
    SQL("SELECT linkedid FROM cel WHERE uniqueid={uniqueid} AND eventtype={eventtype} AND appname={appname} AND context={context} LIMIT 1")
      .on('uniqueid -> uniqueid, 'eventtype -> eventType, 'appname -> appName, 'context -> context)
      .as(get[String]("linkedid").*)
      .headOption
  }

  def getLinkedIdFromUniqueId(uniqueid: String)(implicit c: Connection): Option[String] = {
    SQL("SELECT max(linkedid) as linkedid FROM cel WHERE uniqueid={uniqueid}")
      .on('uniqueid -> uniqueid)
      .as(get[Option[String]]("linkedid").*)
      .headOption.flatten
  }

  def getPendingCelsByUniqueIds(uniqueIds: List[String], startId: Int, connection: Connection, batchSize: Int)(implicit materializer: Materializer) = {
    val inClause = SqlUtils.createInClauseOrFalseFast("uniqueid", uniqueIds)

    AkkaStream.source[Cel](
      SQL(
        s"""SELECT
                         id, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context,
                         channame, appname, appdata, amaflags, accountcode, peeraccount, uniqueid, linkedid, userfield, peer, extra
                  FROM
                         cel
                  WHERE
                         $inClause AND id < {startId}
                  ORDER BY id ASC"""
      )
        .on('startId -> startId).withFetchSize(Some(batchSize)), cel)(materializer, connection)
  }

  def getCels(startId: Long, maxId: Long)(implicit c: Connection): List[Cel] = {
    SQL(
      """SELECT
                id, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context,
                channame, appname, appdata, amaflags, accountcode, peeraccount, uniqueid, linkedid, userfield, peer, extra
         FROM
                cel
         WHERE
                id >= {startId} AND id <= {maxId}
         ORDER BY id ASC""".stripMargin)
      .on('startId -> startId, 'maxId -> maxId)
      .as(cel.*)
  }
}
