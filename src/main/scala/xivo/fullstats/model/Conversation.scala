package xivo.fullstats.model

import java.sql.Connection

import anorm.{SQL, ~}
import anorm.SqlParser.get
import org.slf4j.LoggerFactory

object Conversation {
  val conversationInsert = SQL("""
                          INSERT INTO
                                xc_call_conversation(
                                   caller_call_id, callee_call_id)
                          VALUES (
                                   {caller_call_id}, {callee_call_id})""")

  val conversationUpdate = SQL("""
                          UPDATE
                                xc_call_conversation
                          SET
                                caller_call_id={caller_call_id}, callee_call_id={callee_call_id}
                          WHERE id={id}""")


  val simple = get[Long]("id") ~
    get[Long]("caller_call_id") ~
    get[Long]("callee_call_id") map {
    case id ~ callerCallId ~ calleeCallId => Conversation(Some(id), callerCallId, calleeCallId)
  }


  val logger = LoggerFactory.getLogger(getClass)

  def insertOrUpdate(conversation: Conversation)(implicit c: Connection): Conversation = {
    if (conversation.id.isDefined) update(conversation)
    else insert(conversation)
  }

  def insert(conversation: Conversation)(implicit c: Connection): Conversation = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Insert Conversation - $conversation")
      val id: Option[Long] = conversationInsert.on(
        'caller_call_id -> conversation.callerCallId, 'callee_call_id -> conversation.calleeCallId).executeInsert()
      c.commit()
      c.setAutoCommit(true)
      conversation.copy(id = id.map(_.toInt))
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the conversation ", e)
        c.setAutoCommit(true)
        conversation
    }
  }

  def update(conversation: Conversation)(implicit c: Connection): Conversation = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Update Conversation - $conversation")
      conversationUpdate.on(
        'caller_call_id -> conversation.callerCallId, 'callee_call_id -> conversation.calleeCallId, 'id -> conversation.id).executeInsert()
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the conversation ", e)
        c.setAutoCommit(true)
    }
    conversation
  }

  def getAll()(implicit c: Connection): List[Conversation] = {
    SQL("SELECT id, caller_call_id, callee_call_id FROM xc_call_conversation".stripMargin)
      .as(simple.*)
  }
}

case class Conversation(id: Option[Long], callerCallId: Long, calleeCallId: Long) extends CallThreadEvent

case class ConversationLinks(linkedId: String, relatedUniqueIds: List[String]) {

  private def getCallChannelIds(uniqueIds: List[String])(implicit c: Connection): List[Long] = {
    CallChannel.getIdByuniqueIds(uniqueIds)
  }

  def getCalledCallChannelIds(implicit c: Connection): List[Long] = getCallChannelIds(relatedUniqueIds)

  def getCallerCallChannelId(implicit c: Connection): Option[Long] = getCallChannelIds(List(linkedId)).headOption
}


