package xivo.fullstats.model

import org.joda.time.DateTime

abstract class CallEvent(val id: Int, val callid: String, val time: DateTime)
