package xivo.fullstats.model

import java.sql.Connection

import anorm.SqlParser.get
import anorm.{SQL, ~}

case class Extension(id: Option[Long], commented: Int, context: String, exten: String, extType: String, typeval: String)

object Extension {

  val simple = get[Long]("id") ~
    get[Int]("commented") ~
    get[String]("context") ~
    get[String]("exten") ~
    get[String]("type") ~
    get[String]("typeval") map {
    case id ~ commented ~ context ~ exten ~ extType ~ typeval =>
      Extension(Some(id), commented, context, exten, extType, typeval)
  }

  def getExtensionByNumber(number: String)(implicit c: Connection): Option[Extension] = {
    SQL(
      """SELECT
                id, commented, context, exten, type, typeval
         FROM
                extensions
         WHERE
                exten = {number}
             AND
                type = 'user'
             AND
                commented = 0
         LIMIT 1""".stripMargin)
      .on('number -> number).as(simple *).headOption
  }
}
