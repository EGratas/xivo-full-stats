package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.Column.nonNull
import anorm.SqlParser.get
import anorm.{Column, MetaDataItem, SQL, TypeDoesNotMatch, ~}
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import xivo.fullstats.model
import xivo.fullstats.model.QueueTerminationType.QueueTerminationType

object QueueTerminationType extends Enumeration {
  type QueueTerminationType = TerminationType

  case class TerminationType(terminationEvent: String, terminationType: String) extends Val(terminationEvent)

  private def terminationTypeHelper(terminationEvent: String, terminationType: String) = TerminationType(terminationEvent, terminationType)

  val COMPLETEAGENT = terminationTypeHelper("COMPLETEAGENT", "answered")
  val COMPLETECALLER = terminationTypeHelper("COMPLETECALLER", "answered")
  val ABANDON = terminationTypeHelper("ABANDON", "abandoned")
  val EXITWITHKEY = terminationTypeHelper("EXITWITHKEY", "exit_with_key")
  val EXITWITHTIMEOUT = terminationTypeHelper("EXITWITHTIMEOUT", "timeout")
  val CLOSED = terminationTypeHelper("CLOSED", "closed")
  val DIVERT_CA_RATIO = terminationTypeHelper("DIVERT_CA_RATIO", "divert_ca_ratio")
  val DIVERT_HOLDTIME = terminationTypeHelper("DIVERT_HOLDTIME", "divert_waittime")
  val FULL = terminationTypeHelper("FULL", "full")
  val LEAVEEMPTY = terminationTypeHelper("LEAVEEMPTY", "leaveempty")
  val JOINEMPTY = terminationTypeHelper("JOINEMPTY", "joinempty")
  val ATTENDEDTRANSFER = terminationTypeHelper("ATTENDEDTRANSFER", "answered")
  val BLINDTRANSFER = terminationTypeHelper("BLINDTRANSFER", "answered")
  val CONNECT = terminationTypeHelper("CONNECT", "answered")
  val SYSTEM_HANGUP = terminationTypeHelper("SYSTEM", "system_hangup")
  val STALLED = terminationTypeHelper("STALLED", "stalled")

  val terminationValues: Map[model.QueueTerminationType.QueueTerminationType, String] = Map(
    COMPLETEAGENT -> "answered",
    COMPLETECALLER -> "answered",
    ABANDON -> "abandoned",
    EXITWITHKEY -> "exit_with_key",
    EXITWITHTIMEOUT -> "timeout",
    CLOSED -> "closed",
    DIVERT_CA_RATIO -> "divert_ca_ratio",
    DIVERT_HOLDTIME -> "divert_waittime",
    FULL -> "full",
    LEAVEEMPTY -> "leaveempty",
    JOINEMPTY -> "joinempty",
    ATTENDEDTRANSFER -> "answered",
    CONNECT -> "answered",
    BLINDTRANSFER -> "answered",
    SYSTEM_HANGUP -> "system_hangup",
    STALLED -> "stalled"
  )
}

object QueueCall {
  val logger = LoggerFactory.getLogger(getClass)

  implicit val columnToTerminationType: Column[QueueTerminationType] = nonNull { (value, meta) =>
    val MetaDataItem(termination, _, _) = meta
    value match {
      case s: String =>
        QueueTerminationType.terminationValues.find(t => t._2 == s).map(_._1) match {
          case Some(t) => Right(t)
          case None => Left(TypeDoesNotMatch(s"Unable to convert $value: ${value.asInstanceOf[AnyRef].getClass} to enum $termination"))
        }
      case _ => Left(TypeDoesNotMatch(s"Unable to convert $value: ${value.asInstanceOf[AnyRef].getClass} to enum $termination"))
    }
  }

  val simple = get[Long]("id") ~
    get[String]("queue_ref") ~
    get[DateTime]("start_time") ~
    get[Option[DateTime]]("end_time") ~
    get[Option[DateTime]]("answer_time") ~
    get[Option[Long]]("ring_duration") ~
    get[Option[QueueTerminationType]]("termination") ~
    get[Option[Long]]("agent_id") ~
    get[String]("unique_id") map {
    case id ~ queueRef ~ startTime ~ endTime ~ answerTime ~ ringDuration ~ termination ~ agentId ~ uniqueId =>
      QueueCall(Some(id), queueRef, startTime, endTime, termination, answerTime, ringDuration, agentId, uniqueId, false, Map.empty)
  }

  val queueCallInsert = SQL("""
                          INSERT INTO
                                xc_queue_call(
                                   queue_ref, start_time, end_time, termination, answer_time, ring_duration, agent_id, unique_id)
                          VALUES (
                                   {queue_ref}, {start_time}, {end_time}, {termination}::queue_termination_type, {answer_time},
                                   {ring_duration}, {agent_id}, {unique_id})""")

  val queueCallUpdate = SQL("""
                          UPDATE
                                xc_queue_call
                          SET
                                  queue_ref={queue_ref}, start_time={start_time}, end_time={end_time},
                                  termination={termination}::queue_termination_type, answer_time={answer_time}, ring_duration={ring_duration},
                                  agent_id={agent_id}, unique_id={unique_id}
                          WHERE id={id}""")


  def insertOrUpdate(queueCall: QueueCall)(implicit c: Connection): QueueCall = {
    if (queueCall.id.isDefined) update(queueCall)
    else insert(queueCall)
  }

  def insert(queueCall: QueueCall)(implicit c: Connection): QueueCall = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Insert QueueCall - $queueCall")
      val id: Option[Long] = queueCallInsert.on(
        'queue_ref -> queueCall.queueRef, 'start_time -> queueCall.startTime.toDate, 'end_time -> queueCall.endTime.map(_.toDate),
        'termination -> queueCall.termination.map(_.terminationType.toString), 'answer_time -> queueCall.answerTime.map(_.toDate), 'ring_duration -> queueCall.ringDuration,
        'agent_id -> queueCall.agentId, 'unique_id -> queueCall.uniqueId).executeInsert()
      c.commit()
      c.setAutoCommit(true)
      queueCall.copy(id = id.map(_.toInt))
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the queue call ", e)
        c.setAutoCommit(true)
        queueCall
    }
  }

  def update(queueCall: QueueCall)(implicit c: Connection): QueueCall = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Update QueueCall - $queueCall")
      queueCallUpdate.on(
        'queue_ref -> queueCall.queueRef, 'start_time -> queueCall.startTime.toDate, 'end_time -> queueCall.endTime.map(_.toDate),
        'termination -> queueCall.termination.map(_.terminationType.toString), 'answer_time -> queueCall.answerTime.map(_.toDate), 'ring_duration -> queueCall.ringDuration,
        'agent_id -> queueCall.agentId, 'unique_id -> queueCall.uniqueId, 'id -> queueCall.id).executeInsert()
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the queue call ", e)
        c.setAutoCommit(true)
    }
    queueCall
  }

  def getByUniqueId(uniqueId: String)(implicit c: Connection): Option[QueueCall] = {
    SQL("SELECT id, queue_ref, start_time, end_time, answer_time, ring_duration, termination, agent_id, unique_id FROM xc_queue_call WHERE unique_id = {uniqueId} LIMIT 1")
      .on('uniqueId -> uniqueId).as(simple *).headOption
  }

  def getQueueCallIdByUniqueId(uniqueId: String, time: DateTime)(implicit c: Connection): Option[Long] = {
    SQL("SELECT id FROM xc_queue_call WHERE unique_id = {uniqueId} AND start_time <= {time} ORDER BY id DESC LIMIT 1")
      .on('uniqueId -> uniqueId, 'time -> time.toDate).as(get[Long]("id").*).headOption
  }

  def getByQueueId(id: Long)(implicit c: Connection): Option[QueueCall] = {
    SQL("SELECT id, queue_ref, start_time, end_time, answer_time, ring_duration, termination, agent_id, unique_id FROM xc_queue_call WHERE id = {id} LIMIT 1")
      .on('id -> id).as(simple.*).headOption
  }

  def setStalePendingCalls()(implicit conn: Connection) =
    SQL("SELECT max(start_time) AS val FROM xc_queue_call").as(get[Option[Date]]("val").single)
    .map { date =>
      SQL("UPDATE xc_queue_call SET end_time = start_time , termination = {termination}::queue_termination_type WHERE end_time IS NULL AND start_time < {maxDate}::timestamp - INTERVAL '1 day'")
        .on('maxDate -> date, 'termination -> Some(QueueTerminationType.STALLED).map(_.terminationType.toString)).executeUpdate()
    }

  def deleteById(uniqueIds: List[String])(implicit conn: Connection) = {
    val inClause = SqlUtils.createInClauseOrFalseFast("unique_id", uniqueIds)
    SQL(s"DELETE FROM xc_queue_call WHERE $inClause").executeUpdate()
  }

  def getPendingUniqueIds(oldestStartTime: DateTime)(implicit conn: Connection): List[String] =
    SQL(s"SELECT unique_id FROM xc_queue_call WHERE start_time >= {oldestStartTime}")
      .on('oldestStartTime -> oldestStartTime.toDate)
      .as(get[String]("unique_id").*)

  def getPendingStartTime()(implicit c: Connection): Option[DateTime] =
    SQL(
      s"""SELECT
                  MIN(start_time) as min_end_time
          FROM
                  xc_queue_call
          WHERE
                  end_time IS null AND start_time >= (SELECT max(start_time) as max FROM xc_queue_call) - INTERVAL '1 day'""".stripMargin)
      .as(get[Option[DateTime]]("min_end_time").single)
}

case class QueueCall(id: Option[Long], queueRef: String, startTime: DateTime, endTime: Option[DateTime], termination: Option[QueueTerminationType],
                     answerTime: Option[DateTime], ringDuration: Option[Long], agentId: Option[Long], uniqueId: String,
                     deleted: Boolean, transferredCalls: Map[String, String])
  extends CallThreadEvent {

  def asNotDeleted = {
    this.copy(deleted = false)
  }

  def asDeleted = {
    this.copy(deleted = true)
  }
}
