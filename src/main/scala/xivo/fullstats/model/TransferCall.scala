package xivo.fullstats.model

import java.sql.Connection

import anorm.{SQL, ~}
import anorm.SqlParser.get
import org.joda.time.DateTime
import org.slf4j.LoggerFactory


object TransferCall {
  val transferCallInsert = SQL("""
                          INSERT INTO
                                xc_call_transfer(
                                   transfer_time, initial_call_id, destination_call_id, talking_call_id,
                                   onhold_call_id)
                          VALUES (
                                   {transfer_time}, {initial_call_id}, {destination_call_id}, {talking_call_id},
                                   {onhold_call_id})""")

  val transferCallUpdate = SQL("""
                          UPDATE
                                xc_call_transfer
                          SET
                                  transfer_time={transfer_time}, initial_call_id={initial_call_id}, destination_call_id={destination_call_id},
                                  talking_call_id={talking_call_id}, onhold_call_id={onhold_call_id}
                          WHERE id={id}""")


  val simple = get[Long]("id") ~
    get[Long]("initial_call_id") ~
    get[Long]("destination_call_id") ~
    get[Long]("talking_call_id") ~
    get[Option[Long]]("onhold_call_id") ~
    get[DateTime]("transfer_time") map {
    case id ~ init ~ end ~ hold ~ talk ~ time => TransferCall(Some(id), init, end, hold, talk, time)
  }

  val logger = LoggerFactory.getLogger(getClass)

  def insertOrUpdate(transferCall: TransferCall)(implicit c: Connection): TransferCall = {
    if (transferCall.id.isDefined) update(transferCall)
    else insert(transferCall)
  }

  def insert(transferCall: TransferCall)(implicit c: Connection): TransferCall = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Insert TransferCall - $transferCall")
      val id: Option[Long] = transferCallInsert.on(
        'transfer_time -> transferCall.transferTime.toDate, 'initial_call_id -> transferCall.init,
        'destination_call_id -> transferCall.end, 'talking_call_id -> transferCall.talk,
        'onhold_call_id -> transferCall.hold).executeInsert()
      c.commit()
      c.setAutoCommit(true)
      transferCall.copy(id = id.map(_.toInt))
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the transfer call ", e)
        c.setAutoCommit(true)
        transferCall
    }
  }

  def update(transferCall: TransferCall)(implicit c: Connection): TransferCall = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Update TransferCall - $transferCall")
      transferCallUpdate.on(
        'transfer_time -> transferCall.transferTime.toDate, 'initial_call_id -> transferCall.init,
        'destination_call_id -> transferCall.end, 'talking_call_id -> transferCall.talk,
        'onhold_call_id -> transferCall.hold, 'id -> transferCall.id).executeInsert()
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the call data ", e)
        c.setAutoCommit(true)
    }
    transferCall
  }

  def getIdByAsteriskIds(asteriskIds: List[String], answered: Boolean)(implicit c: Connection): List[Long] = {
    SQL("SELECT id FROM xc_call_channel WHERE asterisk_id IN ({asteriskIds}) AND answered = {answered}")
      .on('asteriskIds -> asteriskIds, 'answered -> answered).as(get[Long]("id").*)
  }

  def getAll()(implicit c: Connection): List[TransferCall] = {
    SQL(
      """SELECT id, transfer_time, initial_call_id, destination_call_id, talking_call_id,
                onhold_call_id FROM xc_call_transfer""".stripMargin)
      .as(simple.*)
  }
}

case class TransferCall(id: Option[Long], init: Long, end: Long, hold: Long, talk: Option[Long], transferTime: DateTime) extends CallThreadEvent

case class TransferLinks(linkedId: String, collectedUniqueIds: List[String], transferDecisionId: Option[String],
                         onHold: Option[String], transferTime: Option[DateTime],
                         doSaveToDatabase: Boolean = false, stopCollecting: Boolean = false)
