package xivo.fullstats

import java.util.InvalidPropertiesFormatException

import org.joda.time.{DateTime, Period}

object TimeUtils {

  val intervalStr = "(\\d+):(\\d+):(\\d+)".r

  def parseInterval(s: String): Period = {
    intervalStr.findAllIn(s).matchData foreach {
      m => return new Period(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), 0)
    }
    throw new InvalidPropertiesFormatException(s"Found : $s, expected : hh:mm:ss")
  }

  def roundToInterval(time: DateTime, interval: Period): DateTime = time.minus(new Period((time.getSecondOfDay % interval.toStandardSeconds.getSeconds)*1000))
    .withMillisOfSecond(0)

  def max(d1: DateTime, d2: DateTime): DateTime = if(d1.isAfter(d2)) d1 else d2

  def min(d1: DateTime, d2: DateTime): DateTime = if(d1.isAfter(d2)) d2 else d1
}
