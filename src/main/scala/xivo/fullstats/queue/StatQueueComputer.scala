package xivo.fullstats.queue

import org.joda.time.{DateTime, Period}
import xivo.fullstats.TimeUtils
import xivo.fullstats.model.{QueueLog, StatQueuePeriodic}

class StatQueueComputer(val queue: String, var lastPeriod: StatQueuePeriodic, interval: Period) {
  def notifyNewInterval(date: DateTime): Option[StatQueuePeriodic] = {
    var res: Option[StatQueuePeriodic] = None
    if(!lastPeriod.isEmpty)
      res = Some(lastPeriod)
    lastPeriod = StatQueuePeriodic(TimeUtils.roundToInterval(date, interval), queue)
    res
  }

  def processQueueLog(ql: QueueLog): Unit = {
    ql.event match {
      case "ENTERQUEUE" => lastPeriod.total += 1
      case "CLOSED" => lastPeriod.closed += 1
      case "FULL" => lastPeriod.full += 1
      case "DIVERT_CA_RATIO" => lastPeriod.divertCaRatio += 1
      case "DIVERT_HOLDTIME" => lastPeriod.divertWaitTime += 1
      case "JOINEMPTY" => lastPeriod.joinEmpty += 1
      case "ABANDON" => lastPeriod.abandoned += 1
      case "CONNECT" => lastPeriod.answered += 1
      case "EXITWITHTIMEOUT" => lastPeriod.timeout += 1
      case "LEAVEEMPTY" => lastPeriod.leaveEmpty += 1
      case "EXITWITHKEY" => lastPeriod.exitWithKey += 1
      case _ =>
    }
  }

}
