package xivo.fullstats.queue

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import org.slf4j.LoggerFactory
import xivo.fullstats.model.{QueueLog, StatQueuePeriodic, StatQueuePeriodicManager, StatQueuePeriodicManagerImpl}
import xivo.fullstats.{EventParser, TimeUtils}

import scala.collection.mutable

class StatQueueManager(interval: Period, minId: Int, statQueuePeriodicManager: StatQueuePeriodicManager = StatQueuePeriodicManagerImpl)(implicit conn: Connection) extends EventParser[QueueLog] {

  val queues = mutable.Map[String, StatQueueComputer]()
  val QueueEvents = List("ENTERQUEUE", "CLOSED", "FULL", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "JOINEMPTY",  "ABANDON",
  "CONNECT", "EXITWITHTIMEOUT", "LEAVEEMPTY", "EXITWITHKEY")
  val logger = LoggerFactory.getLogger(getClass)
  var currentInterval: Option[DateTime] = None

  override def saveState(): Unit = {
    val periods = queues.values.map(_.lastPeriod)
    logger.info(s"Saving ongoing stat_queue_periodic: $periods")
    statQueuePeriodicManager.insert(periods)
  }

  override def parseEvent(e: QueueLog): Unit = {
    val roundedTime = TimeUtils.roundToInterval(e.time, interval)
    if(currentInterval.isEmpty)
      currentInterval = Some(roundedTime)
    else if(currentInterval.get.isBefore(roundedTime)) {
      queues.values.foreach(_.notifyNewInterval(e.time).foreach(sqp => {
        logger.info(s"Inserting stat queue periodic $sqp")
        statQueuePeriodicManager.insertOrUpdate(sqp)
      }))
      currentInterval = Some(roundedTime)
    }
    if(QueueEvents.contains(e.event) && e.id >= minId)
      realParseEvent(e)
  }

  def realParseEvent(ql: QueueLog): Unit = {
    if(!queues.contains(ql.queueName)) {
      logger.info(s"Processing a new queue from the queue_log $ql")
      processNewQueue(ql)
    }
    queues(ql.queueName).processQueueLog(ql)
  }

  def processNewQueue(ql: QueueLog): Unit = {
    val periodInterval = TimeUtils.roundToInterval(ql.time, interval)

    val lastPeriod = statQueuePeriodicManager.lastForQueue(ql.queueName) match {
      case Some(period) if period.time.isEqual(periodInterval) => period
      case _ => StatQueuePeriodic(periodInterval, ql.queueName)
    }
    queues.put(ql.queueName, new StatQueueComputer(ql.queueName, lastPeriod, interval))
  }
}
