CREATE TABLE queue_threshold_time (
    queue_ref VARCHAR(128) UNIQUE NOT NULL,
    t1 INTEGER,
    t2 INTEGER
);
