ALTER TABLE xc_call_channel
      ADD COLUMN call_thread_id INTEGER,
      ADD COLUMN call_thread_seq INTEGER;
