package xivo.fullstats.integration

import xivo.fullstats.CallEventsLoop
import xivo.fullstats.callparsers.CallDataGenerator
import xivo.fullstats.model._
import xivo.fullstats.testutils.{CelTestUtils, DBTest, DBUtil}

class CallDataIntegrationSpec extends DBTest(List("attached_data", "hold_periods", "call_data", "agent_position", "call_element")) {

  var loop: CallEventsLoop = null
  DBUtil.insertFromCsv()

  override def beforeEach() {
    super.beforeEach()
    loop = new CallEventsLoop(CelTestUtils.eventsByAscendingId(), List(new CallDataGenerator), List())
  }



  "A CallDataGenerator" should "retrieve uniqueid, src_num, dst_num, call_direction, start time, answer time, hangup time for each call" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    res.size shouldEqual 13
    for (callData <- res) {
      callData.uniqueId match {
        case "1442840478.9" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:01:18.248")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:01:20.358"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:01:22.477"))

        case "1442840487.11" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual None
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:01:27.521")
          callData.answerTime shouldEqual None
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:01:35.399"))

        case "1442840677.13" => callData.dstNum shouldEqual Some("51002")
          callData.callDirection shouldEqual CallDirection.Incoming
          callData.srcNum shouldEqual Some("0230210082")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:04:37.775")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:04:40.617"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:05:20.357"))

        case "1442840867.17" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:07:47.339")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:07:51.577"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:07:58.140"))

        case "1442840931.19" => callData.dstNum shouldEqual Some("51200")
          callData.callDirection shouldEqual CallDirection.Incoming
          callData.srcNum shouldEqual Some("0230210082")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:08:51.750")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:09:16.007"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:09:19.339"))

        case "1442841214.29" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:13:34.226")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:13:35.528"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:13:50.643"))

        case "1442841272.32" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:14:32.460")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:14:33.719"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:15:09.602")) // ATTENDEDTRANSFER time (l.885 in csv)

        case "1442841338.37" => callData.dstNum shouldEqual Some("1001")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:15:38.498")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:15:39.598"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:16:10.369"))

        case "1442842056.41" => callData.dstNum shouldEqual Some("0230210082")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("1002")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-21 15:27:36.255")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:27:38.929"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-21 15:27:41.436"))

        case "1443015814.234" => callData.dstNum shouldEqual Some("0230210082")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("1001")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-23 15:43:34.392")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-23 15:43:37.623"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-23 15:43:40.192"))

        case "1443016357.240" => callData.dstNum shouldEqual Some("1003")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1001")
          callData.status shouldEqual Some("answer")
          callData.startTime shouldEqual formatMs.parseDateTime("2015-09-23 15:52:37.593")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2015-09-23 15:52:40.221"))
          callData.endTime shouldEqual Some(formatMs.parseDateTime("2015-09-23 15:52:45.448"))

        case "1612948555.8" =>          
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("1011")
          callData.dstNum shouldEqual Some("77001")
          callData.status shouldEqual Some("answer")
          callData.answerTime shouldEqual Some(formatMs.parseDateTime("2021-02-10 10:15:57.976"))

        case _ =>
      }
    }

  }

  it should "update agent source and destination when call starts" in {
    List(
      AgentPosition("1000", "2027", None, format.parseDateTime("2014-01-01 08:00:00"), None),
      AgentPosition("1100", "2015", Some("52015"), format.parseDateTime("2014-01-01 08:00:00"), None),
      AgentPosition("5000", "1000", None, format.parseDateTime("2015-05-05 08:00:00"), None)).foreach( AgentPosition.insertOrUpdate)


    loop.processNewCallEvents()

    val res = CallDataTestUtils.allCallData
    for (callData <- res) {
      callData.uniqueId match {
        case "1457450350.49" =>
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual Some("1100")
        case _ =>
      }
    }

  }

  it should "extract the ring on answer duration" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData

    for (callData <- res) {
      callData.uniqueId match {
        case "1442840478.9" => callData.ringDurationOnAnswer shouldEqual Some(1)
        case "1442840487.11" => callData.ringDurationOnAnswer shouldEqual None
        case "1442840677.13" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1442840867.17" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1442840931.19" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1442841214.29" => callData.ringDurationOnAnswer shouldEqual Some(1)
        case "1442841272.32" => callData.ringDurationOnAnswer shouldEqual Some(1)
        case "1442841338.37" => callData.ringDurationOnAnswer shouldEqual Some(0)
        case "1442842056.41" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1443015814.234" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1443016357.240" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case _ =>
      }
    }
  }

  it should "put a flag on a call if it has been transfered" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData

    for (callData <- res) {
      callData.uniqueId match {
        case "1442841214.29" | "1442841272.32" | "1442841338.37" => callData.transfered shouldBe true
        case "1442840478.9" | "1442840487.11" | "1442840677.13" | "1442840867.17" | "1442840931.19" | "1442842056.41" | "1443015814.234" | "1443016357.240" =>
          callData.transfered shouldBe false
        case _ =>
      }
    }
  }

  it should "extract the hold periods" in {
    loop.processNewCallEvents()
    val callDatas = CallDataTestUtils.allCallData

    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1442840867.17" => callData.holdPeriods shouldEqual List(
          HoldPeriod(formatMs.parseDateTime("2015-09-21 15:07:53.698"), Some(formatMs.parseDateTime("2015-09-21 15:07:56.798")), "1442840867.17"))
        case "1442841338.37" => callData.holdPeriods shouldEqual List(
          HoldPeriod(formatMs.parseDateTime("2015-09-21 15:15:46.601"), Some(formatMs.parseDateTime("2015-09-21 15:16:04.444")), "1442841338.37"))
        case "1442841214.29" | "1442840478.9" | "1442840487.11" | "1442840677.13" | "1442840931.19" | "1442841272.32" | "1442842056.41" | "1443015814.234" | "1443016357.240"=>
          callData.holdPeriods.size shouldEqual 0
        case _ =>
      }
    }
  }

  it should "extract call elements" in {

    loop.processNewCallEvents()

    for(callData <- CallDataTestUtils.allCallData) {
      callData.uniqueId match {
        case "1442840931.19" => CallElementTestUtils.byCallDataId(callData.id.get).map(_.copy(id=None)) shouldEqual List(
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:08:53.554"), None,
            Some(formatMs.parseDateTime("2015-09-21 15:09:08.523")), "SIP/zcevo9", None),
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:09:13.539"), Some(formatMs.parseDateTime("2015-09-21 15:09:16.007")),
            Some(formatMs.parseDateTime("2015-09-21 15:09:19.327")), "SIP/4qh8dq", None))
        case "1442841214.29" => CallElementTestUtils.byCallDataId(callData.id.get).map(_.copy(id=None)) shouldEqual List(
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:13:34.353"), Some(formatMs.parseDateTime("2015-09-21 15:13:35.528")),
            Some(formatMs.parseDateTime("2015-09-21 15:13:42.532")), "SIP/zcevo9", None),
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:13:42.688"), Some(formatMs.parseDateTime("2015-09-21 15:13:47.345")),
            Some(formatMs.parseDateTime("2015-09-21 15:13:50.628")), "SIP/4qh8dq", None))
        case "1442841272.32" => CallElementTestUtils.byCallDataId(callData.id.get).map(_.copy(id=None)) shouldEqual List(
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:14:32.572"), Some(formatMs.parseDateTime("2015-09-21 15:14:33.719")),
            Some(formatMs.parseDateTime("2015-09-21 15:15:09.604")), "SIP/zcevo9", None),
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-09-21 15:14:37.192"), Some(formatMs.parseDateTime("2015-09-21 15:14:39.740")),
            Some(formatMs.parseDateTime("2015-09-21 15:15:23.352")), "SIP/4qh8dq", None))
        case _ =>
      }
    }
  }

  it should "extract transfer from extra" in {

    loop.processNewCallEvents()
    for(callData <- CallDataTestUtils.allCallData) {
      callData.uniqueId match {
        case "1442841272.32" => callData.transfers shouldEqual
          List(Transfers("1442841272.32","1442841277.34"), Transfers("1442841272.32","1442841272.32"))
        case _ =>
      }
    }
  }
}
