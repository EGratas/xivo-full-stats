package xivo.fullstats.integration

import xivo.fullstats.model._
import xivo.fullstats.callparsers.CallOnQueueGenerator
import xivo.fullstats.testutils.{CelTestUtils, DBTest, DBUtil}
import xivo.fullstats.{CallEventsLoop, EmptyObjectProvider}

class CallOnQueueIntegrationSpec extends DBTest(List("queue_log", "call_on_queue", "last_queue_log_id")) {

  val callid1 = "123456.789"
  val callid2 = "123789.456"
  val callid3 = "123901.234"
  val callid4 = "123234.123"

  implicit val c = DBUtil.getConnection
  CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=1, uniqueId=callid1, linkedId=callid1))
  CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=2, uniqueId=callid2, linkedId=callid2))

  "A CallOnQueueGenerator" should "parse abandoned calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00"), callid1, "queue01", "NONE", "ABANDON", None, None, Some("300"), None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:05:00"), callid2, "queue01", "Agent/100", "AGENTCALLBACKLOGIN", None, None, None, None, None)

    new CallEventsLoop(List(ql1, ql2, ql3), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:00")), Some(CallExitType.Abandoned), "queue01", None))
  }

  it should "parse timeout calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00"), callid1, "queue01", "NONE", "EXITWITHTIMEOUT", None, None, Some("300"), None, None)

    new CallEventsLoop(List(ql1, ql2), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:00")), Some(CallExitType.Timeout), "queue01", None))
  }

  it should "parse calls on a closed queue" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "CLOSED", None, None, None, None, None)

    new CallEventsLoop(List(ql1), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 0,
      None, Some(format.parseDateTime("2013-01-01 08:00:00")), Some(CallExitType.Closed), "queue01", None))
  }

  it should "parse answered calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00"), callid1, "queue01", "Agent/2000", "CONNECT", Some("300"), Some(callid2), Some("8"), None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:07:30"), callid1, "queue01", "Agent/2000", "COMPLETEAGENT", Some("300"), Some("240"), Some("3"), None, None)

    new CallEventsLoop(List(ql1, ql2, ql3), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 8,
      Some(format.parseDateTime("2013-01-01 08:05:00")), Some(format.parseDateTime("2013-01-01 08:07:30")),
      Some(CallExitType.Answered), "queue01", Some("2000")))
  }

  it should "parse non-consecutive QueueLogs of different calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:01:00"), callid2, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:05:00"), callid1, "queue01", "NONE", "ABANDON", None, None, Some("300"), None, None)
    val ql4 = QueueLog(4, format.parseDateTime("2013-01-01 08:06:00"), callid2, "queue01", "NONE", "EXITWITHTIMEOUT", None, None, Some("300"), None, None)

    new CallEventsLoop(List(ql1, ql2, ql3, ql4), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 0, None,
        Some(format.parseDateTime("2013-01-01 08:05:00")), Some(CallExitType.Abandoned), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 08:01:00"), 0, None,
        Some(format.parseDateTime("2013-01-01 08:06:00")), Some(CallExitType.Timeout), "queue01", None)
    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(call1, call2)
  }

  it should "extract 2 CallOnQueue when the same call was distributed on 2 queues" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "CLOSED", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:00:01"), callid1, "queue02", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:05:01"), callid1, "queue02", "NONE", "ABANDON", None, None, Some("300"), None, None)

    new CallEventsLoop(List(ql1, ql2, ql3), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:00"), 0, None, Some(format.parseDateTime("2013-01-01 08:00:00")), Some(CallExitType.Closed), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 08:00:01"), 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:01")), Some(CallExitType.Abandoned), "queue02", None)
    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(call1, call2)
  }

  it should "extract 2 CallOnQueue when the same call was forwarded on another queue" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), callid1, "queue01", "NONE", "ENTERQUEUE", None, Some("1001"), Some("1"), None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:01:00"), callid1, "queue01", "Agent/2000", "CONNECT", Some("6"), Some(callid2), Some("8"), None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:02:00"), callid1, "queue01", "Agent/2000", "BLINDTRANSFER", Some("3002"), Some("default"), Some("6"), Some("5"), Some("1"))
    val ql4 = QueueLog(4, format.parseDateTime("2013-01-01 08:03:00"), callid1, "queue02", "NONE", "ENTERQUEUE", None, Some("1001"), Some("1"), None, None)
    val ql5 = QueueLog(4, format.parseDateTime("2013-01-01 08:04:00"), callid1, "queue02", "Agent/2000", "CONNECT", Some("8"), Some(callid3), Some("8"), None, None)
    val ql6 = QueueLog(4, format.parseDateTime("2013-01-01 08:05:00"), callid1, "queue02", "Agent/2000", "COMPLETEAGENT", Some("8"), Some("2"), Some("1"), None, None)


    new CallEventsLoop(List(ql1, ql2, ql3, ql4, ql5, ql6), List(), List(new CallOnQueueGenerator())).processNewCallEvents()

    val call1 = CallOnQueue(None, Some(callid1),
      format.parseDateTime("2013-01-01 08:00:00"), 8,
      Some(format.parseDateTime("2013-01-01 08:01:00")),
      Some(format.parseDateTime("2013-01-01 08:02:00")),
      Some(CallExitType.Answered), "queue01", Some("2000"))

    val call2 = CallOnQueue(None, Some(callid1),
      format.parseDateTime("2013-01-01 08:03:00"), 8,
      Some(format.parseDateTime("2013-01-01 08:04:00")),
      Some(format.parseDateTime("2013-01-01 08:05:00")),
      Some(CallExitType.Answered), "queue02", Some("2000"))

    CallOnQueueTestUtils.allCallOnQueues().map(_.copy(id=None)) should contain theSameElementsAs List(call1, call2)
  }
}

