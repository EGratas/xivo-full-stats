package xivo.fullstats.testutils

import scala.util.Random
import xivo.fullstats.model.{CallChannel, CallScope, CallTermination, CallType, QueueCall}
import org.joda.time.DateTime
import xivo.fullstats.model.CallScope.CallScope
import xivo.fullstats.model.CallTermination.CallTermination
import xivo.fullstats.model.CallType.CallType
import xivo.fullstats.model.CelAppName.AppName
import xivo.fullstats.model.QueueTerminationType.QueueTerminationType


object CallTestUtils {

  def createCallChannel(id: Option[Long] = Some(Random.nextLong()),
                        startTime: DateTime = DateTime.now,
                        endTime: Option [DateTime] = Some(DateTime.now.plusMinutes(5)),
                        emitted: Boolean = false,
                        callerNum: Option[String] =  None,
                        calleeNum: Option[String] =  None,
                        answerTime: Option[DateTime] = None,
                        ringDuration: Option[Long] = None,
                        callType: CallType = CallType.Administrative,
                        scope: CallScope = CallScope.Internal,
                        termination: Option[CallTermination] = Some(CallTermination.Hangup),
                        device: Option[String] = None,
                        uniqueId: String = "123456.789",
                        userId: Option[Long] = None,
                        agentId: Option[Long] = None,
                        queueCallId: Option[Long] = None,
                        originalCallId: Option[Long] = None,
                        deleted: Boolean = false,
                        appName: Option[AppName] = None,
                        callThreadId: Option[Long] = None,
                        callThreadSeq: Long = 1): CallChannel
  = CallChannel(id, startTime, endTime, emitted, callerNum, calleeNum, answerTime, ringDuration, callType, scope, termination, device,
                uniqueId, userId, agentId, queueCallId, originalCallId, deleted, appName, callThreadId.orElse(id), callThreadSeq)

  def createQueueCall(id: Option[Long] = Some(Random.nextLong()),
                      queueRef: String = "",
                      startTime: DateTime = DateTime.now,
                      endTime: Option[DateTime] = None,
                      termination: Option[QueueTerminationType] = None,
                      answerTime: Option[DateTime] = None,
                      ringDuration: Option[Long] = None,
                      agentId: Option[Long] = None,
                      uniqueId: String = "123456.789",
                      deleted: Boolean = false,
                      transferredCalls: Map[String, String] = Map.empty) : QueueCall
  = QueueCall(id, queueRef, startTime, endTime, termination, answerTime, ringDuration, agentId, uniqueId, deleted, transferredCalls)
}
