package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.{DateTime, Period}
import xivo.fullstats.testutils.DBTest

class StatQueuePeriodicSpec extends DBTest(List("call_on_queue", "stat_queue_periodic")) {

  val period = new Period(0, 15, 0, 0)

  "The StatQueuePeriodic singleton" should "insert or update a list of StatQueuePeriodic" in {
    val existingSqp = StatQueuePeriodic(
      None,
      format.parseDateTime("2014-01-01 08:00:00"),
      "thequeue",
      10, 1, 13, 0, 0, 0, 0, 0, 0, 1, 0)
    val genId = StatQueuePeriodicTestUtils.insertStatQueuePeriodic(existingSqp)
    val updatedSqp = existingSqp.copy(id=genId.map(_.toInt), answered=11, abandoned=2, total=14, full=1, closed=1, joinEmpty=1,
      leaveEmpty=1, divertCaRatio=3, divertWaitTime=2, timeout=2, exitWithKey=1)
    val otherSqp = StatQueuePeriodic(
      None,
      format.parseDateTime("2014-01-01 08:15:00"),
      "otherqueue",
      9, 2, 14, 1, 2, 3, 4, 5, 6, 7, 8)

    StatQueuePeriodicManagerImpl.insert(List(updatedSqp, otherSqp))

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(updatedSqp.copy(id=None), otherSqp)
  }

  it should "retrieve the last StatQueuePeriodic for a given queue" in {
    val sqp1 = StatQueuePeriodic(
      None,
      format.parseDateTime("2014-01-01 08:00:00"),
      "thequeue",
      10, 1, 13, 0, 0, 0, 0, 0, 0, 1, 0)
    val sqp2 = StatQueuePeriodic(
      None,
      format.parseDateTime("2014-01-01 08:15:00"),
      "thequeue",
      2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0)
    val sqp3 = StatQueuePeriodic(
      None,
      format.parseDateTime("2014-01-01 08:15:00"),
      "the_other_queue",
      1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0)
    List(sqp1, sqp2, sqp3).foreach(StatQueuePeriodicTestUtils.insertStatQueuePeriodic)

    StatQueuePeriodicManagerImpl.lastForQueue("thequeue").map(_.copy(id=None)) shouldEqual Some(sqp2)
  }

  it should "return None if there is non StatQueuePeriodic for this queue" in {
    StatQueuePeriodicManagerImpl.lastForQueue("thequeue") shouldEqual None
  }
}

object StatQueuePeriodicTestUtils {
  private val simple = get[Date]("time") ~
    get[String]("queue") ~
    get[Int]("answered") ~
    get[Int]("abandoned") ~
    get[Int]("total") ~
    get[Int]("full") ~
    get[Int]("closed") ~
    get[Int]("joinempty") ~
    get[Int]("leaveempty") ~
    get[Int]("divert_ca_ratio") ~
    get[Int]("divert_waittime") ~
    get[Int]("timeout") ~
    get[Int]("exit_with_key") map {
    case time ~ queue ~ answered ~ abandoned ~ total ~ full ~ closed ~ joinempty ~ leaveempty
      ~ divertcaratio ~ divertcawaittime ~ timeout ~ exitWithKey =>
      StatQueuePeriodic(None, new DateTime(time), queue, answered, abandoned, total, full, closed, joinempty, leaveempty,
        divertcaratio, divertcawaittime, timeout, exitWithKey)
  }


  def allStatQueuePeriodic()(implicit conn: Connection) = SQL("SELECT * FROM stat_queue_periodic ORDER BY time ASC").as(simple *)

  def insertStatQueuePeriodic(sqp: StatQueuePeriodic)(implicit conn: Connection): Option[Long] = SQL(
    """INSERT INTO stat_queue_periodic(time, queue, answered, abandoned, total, "full", closed, joinempty,
      leaveempty, divert_ca_ratio, divert_waittime, timeout, exit_with_key) VALUES ({time}, {queue}, {answered}, {abandoned}, {total},
      {full}, {closed}, {joinempty}, {leaveempty}, {divert_ca_ratio}, {divert_waittime}, {timeout}, {exitWithKey})""")
    .on('time -> sqp.time.toDate, 'queue -> sqp.queue, 'answered -> sqp.answered, 'abandoned -> sqp.abandoned,
      'total -> sqp.total, 'full -> sqp.full, 'closed -> sqp.closed, 'joinempty -> sqp.joinEmpty, 'leaveempty -> sqp.leaveEmpty,
      'divert_ca_ratio -> sqp.divertCaRatio, 'divert_waittime -> sqp.divertWaitTime, 'timeout -> sqp.timeout, 'exitWithKey -> sqp.exitWithKey)
    .executeInsert()
}