package xivo.fullstats.model

import java.sql.Connection

import anorm.SQL
import xivo.fullstats.testutils.DBTest


class CallOnQueueSpec extends DBTest(List("call_on_queue")) {

  "The CallOnQueue singleton" should "delete the call on queue with the given callids" in {
    val (callid1, callid2, callid3) = ("12346.789", "123456.788", "13456.777")
    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:00:00"), 0, None, None, Some(CallExitType.Full), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, None, "queue01", None)
    val call3 = CallOnQueue(None, Some(callid3), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)

    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.deleteByCallid(List(callid2, callid3))

    CallOnQueueTestUtils.allCallOnQueues shouldEqual List(call1)
  }

  it should "delete nothing if no callid is given" in {
    val call1 = CallOnQueue(None, Some("123456.777"), format.parseDateTime("2013-01-01 00:00:00"), 0, None, None, Some(CallExitType.Full), "queue01", None)
    val call2 = CallOnQueue(None, Some("123456.888"), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, None, "queue01", None)
    val call3 = CallOnQueue(None, Some("123456.999"), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)

    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.deleteByCallid(List())

    CallOnQueueTestUtils.allCallOnQueues shouldEqual List(call1, call2, call3)
  }

  it should "insert CallOnQueues" in {
    val (callid1, callid2, callid3) = ("12346.789", "123456.788", "13456.777")
    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:00:00"), 0, None, None, Some(CallExitType.Full), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, None, "queue01", None)
    val call3 = CallOnQueue(None, Some(callid3), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)

    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueueTestUtils.allCallOnQueues shouldEqual List(call1, call2, call3)
  }

  it should "retrieve pending calls" in {
    val (callid1, callid2) = ("12346.789", "123456.788")
    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:00:00"), 0, None, Some(format.parseDateTime("2013-01-01 00:00:00")), Some(CallExitType.Full), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, Some(CallExitType.Answered), "queue02", None)
    val call3 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)

    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.getPendingCallIds() shouldEqual List(callid1, callid2)
  }

  it should "retrieve CallOnQueues with the same time and callid as the given CallOnQueues, but whose status is not null" in {
    val (callid1, callid2, callid3) = ("12346.789", "123456.788", "123456.787")
    val oldCall1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, None, "queue01", None)
    val oldCall2 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)
    val oldCall3 = CallOnQueue(None, Some(callid3), format.parseDateTime("2013-01-01 00:02:15"), 0, None, None, None, "queue01", None)

    val newCall1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, Some(CallExitType.Abandoned), "queue01", None)
    val newCall2 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, Some(CallExitType.Answered), "queue01", None)
    val newCall3 = CallOnQueue(None, Some(callid3), format.parseDateTime("2013-01-01 00:02:15"), 0, None, None, None, "queue01", None)

    List(newCall1, newCall2, newCall3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.getFormerlyPendingCalls(List(oldCall1, oldCall2, oldCall3)) shouldEqual List(newCall1, newCall2)
  }

  it should "retrieve CallOnQueues with null status" in {
    val (callid1, callid2) = ("12346.789", "123456.788")
    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:00:00"), 0, None, Some(format.parseDateTime("2013-01-01 00:00:00")), Some(CallExitType.Full), "queue01", None)
    val call2 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, Some(CallExitType.Answered), "queue02", None)
    val call3 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-01 00:02:00"), 0, None, None, None, "queue01", None)

    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.getAllWithNullStatus() shouldEqual List(call3)
  }

  it should "delete the pending calls whose date is too old" in {
    val (callid1, callid2) = ("12346.789", "123456.788")
    val call1 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 00:01:00"), 0, None, None, None, "queue01", None)
    val call2 = CallOnQueue(None, Some(callid1), format.parseDateTime("2013-01-01 01:01:00"), 0, None, None, None, "queue02", None)
    val call3 = CallOnQueue(None, Some(callid2), format.parseDateTime("2013-01-02 00:02:00"), 0, None, None, None, "queue01", None)
    List(call1, call2, call3).foreach(CallOnQueue.insertOrUpdate)

    CallOnQueue.deleteStalePendingCalls()

    CallOnQueueTestUtils.allCallOnQueues() shouldEqual List(call2, call3)
  }

  it should "update a call_on_queue if the id is defined" in {
    val origCq = CallOnQueue.insertOrUpdate(CallOnQueue(None, Some("123456.789"), format.parseDateTime("2015-01-01 08:00:00"), 0, None, None, None, "queue01", None))
    val newCq = origCq.copy(hangupTime=Some(format.parseDateTime("2015-01-01 08:15:00")), answerTime = Some(format.parseDateTime("2015-01-01 08:10:00")), agentNum = Some("1000"))
    CallOnQueue.insertOrUpdate(newCq)

    CallOnQueueTestUtils.allCallOnQueues() shouldEqual List(newCq.copy(id=None))

  }

  "The CallOnQueue class" should "check for equality" in {
    val date = format.parseDateTime("2013-01-01 00:02:00")
    val call1 = CallOnQueue(None, Some("123456.789"), date, 1, Some(date), Some(date), None, "queue01", Some("Agent/2000"))
    val call2 = CallOnQueue(None, Some("123456.789"), date, 1, Some(date), Some(date), None, "queue01", Some("Agent/2000"))

    call1 should equal(call2)
  }

  it should "check for unequality" in {
    val d1 = format.parseDateTime("2013-01-01 00:02:00")
    val call1 = CallOnQueue(None, Some("123456.789"), d1, 1, Some(d1), Some(d1), None, "queue01", Some("Agent/2000"))
    val d2 = format.parseDateTime("2013-01-01 00:03:00")
    val call2 = CallOnQueue(None, Some("123456.789"), d2, 1, Some(d2), Some(d2), None, "queue01", Some("Agent/2000"))

    call1 should not equal call2
  }
}

object CallOnQueueTestUtils {

  def allCallOnQueues()(implicit conn: Connection): List[CallOnQueue] =
    SQL("SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar, queue_ref, agent_num FROM call_on_queue").as(CallOnQueue.simple *)
}