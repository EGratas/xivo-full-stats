package xivo.fullstats.model

import anorm._
import org.joda.time.DateTime
import xivo.fullstats.testutils.DBTest

class AgentPositionSpec extends DBTest(List("agent_position")) {

  "The AgentPosition Singleton" should "insert or update a position" in {
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.100"),
      Some(formatMs.parseDateTime("2014-01-01 10:00:00.200"))))
    AgentPosition.insertOrUpdate(AgentPosition("1000", "1000", Some("31000"), formatMs.parseDateTime("2014-01-01 08:00:00.100"),
      Some(formatMs.parseDateTime("2014-01-01 08:00:00.100"))))

    allAgentPositions shouldEqual
      List(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), Some(formatMs.parseDateTime("2014-01-01 10:00:00.200"))),
           AgentPosition("1000", "1000", Some("31000"), formatMs.parseDateTime("2014-01-01 08:00:00.100"), Some(formatMs.parseDateTime("2014-01-01 08:00:00.100"))))
  }

  it should "return the maximum start or end date" in {
    AgentPosition.insertOrUpdate(AgentPosition("1000", "1000", Some("31000"), format.parseDateTime("2014-01-01 08:00:00"),
      Some(format.parseDateTime("2014-01-01 09:00:00"))))
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.100"), None))

    AgentPosition.getMaxDate() shouldEqual Some(formatMs.parseDateTime("2014-01-01 09:15:00.100"))
  }

  it should "return None if there is no data" in {
    AgentPosition.getMaxDate() shouldEqual None
  }

  it should "return the current agent position if it exists in old queue_log format" in {
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))

    AgentPosition.lastPendingForAgent("1100") shouldEqual Some(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))
    AgentPosition.lastPendingForAgent("1200") shouldEqual None
  }

  it should "return the current agent position if it exists in with current queue_log format" in {
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.123"), None))

    AgentPosition.lastPendingForAgent("1100") shouldEqual Some(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.123"), None))
    AgentPosition.lastPendingForAgent("1200") shouldEqual None
  }

  it should "update and not create the current agent position if it exists with current queue_log format" in {
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.123"), None))
    AgentPosition.insertOrUpdate(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.123"), Some(formatMs.parseDateTime("2014-01-01 10:00:00.200"))))

    allAgentPositions shouldEqual
      List(AgentPosition("1100", "1100", Some("31100"), formatMs.parseDateTime("2014-01-01 09:15:00.123"), Some(formatMs.parseDateTime("2014-01-01 10:00:00.200"))))
  }

  it should "return the agent associated to a given line or sda at a given time" in {
    val closedPosition = AgentPosition("1000", "2000", None, new DateTime(format.parseDateTime("2014-01-01 08:00:00")), Some(new DateTime(format.parseDateTime("2014-01-01 18:00:00"))))
    val openedPosition = AgentPosition("1100", "2100", Some("52100"), new DateTime(format.parseDateTime("2014-01-02 08:00:00")), None)
    AgentPosition.insertOrUpdate(closedPosition)
    AgentPosition.insertOrUpdate(openedPosition)

    AgentPosition.agentForNumAndTime("2000", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual Some("1000")
    AgentPosition.agentForNumAndTime("2100", format.parseDateTime("2014-01-02 09:00:00")) shouldEqual Some("1100")
    AgentPosition.agentForNumAndTime("52100", format.parseDateTime("2014-01-02 09:00:00")) shouldEqual Some("1100")
    AgentPosition.agentForNumAndTime("2100", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual None
    AgentPosition.agentForNumAndTime("3000", format.parseDateTime("2014-01-02 09:00:00")) shouldEqual None
  }



  private def allAgentPositions =  SQL("SELECT line_number, sda, agent_num, start_time, end_time FROM agent_position").as(AgentPosition.simple *)
}
