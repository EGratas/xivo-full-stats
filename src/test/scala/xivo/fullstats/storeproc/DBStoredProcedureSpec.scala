package xivo.fullstats.storeproc

import anorm.{SQL, SqlParser}
import xivo.fullstats.testutils.DBTest

class DBStoredProcedureSpec extends DBTest(List ("stat_queue_periodic")) {
  val date1 = "2016-01-01 08:00:05"
  val date2 = "2016-01-01 08:59:59.111"
  val date3 = "2016-01-01 08:59:59.999"

  val dateResultLowerBound = "2016-01-01 08:00:00"
  val dateResultUpperBound = "2016-01-01 09:00:00"

  def sqlQuery(isRound:Boolean, date:String):String = {
    val round = if (isRound) "round" else "floor"

    SQL(s""" SELECT to_char(${round}_to_x_seconds('$date', 3600),'YYYY-MM-DD HH:MI:SS') as result_date """)
      .as(SqlParser.str("result_date").single)
  }

  "The round_to_x stored procedure" should "round to second" in {
    sqlQuery(true,date1) shouldEqual dateResultLowerBound
    sqlQuery(true,date2) shouldEqual dateResultLowerBound
    sqlQuery(true,date3) shouldEqual dateResultUpperBound
  }

  "The floor_to_x stored procedure" should "floor to second" in {
    sqlQuery(false,date1) shouldEqual dateResultLowerBound
    sqlQuery(false,date2) shouldEqual dateResultLowerBound
    sqlQuery(false,date3) shouldEqual dateResultLowerBound
  }
}
