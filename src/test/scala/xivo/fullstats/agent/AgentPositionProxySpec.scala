package xivo.fullstats.agent

import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.model.AgentPosition
import xivo.fullstats.testutils.DBTest

class AgentPositionProxySpec extends DBTest(List("agent_position")) with EasyMockSugar {

  "The AgentPositionProxy" should "register a buffer" in {
    AgentPositionProxy.positionBuffer = None
    val buffer = mock[AgentPositionBuffer]
    AgentPositionProxy.register(buffer)

    AgentPositionProxy.positionBuffer shouldEqual Some(buffer)
  }

  it should "search the position in the buffer" in {
    val buffer = mock[AgentPositionBuffer]
    AgentPositionProxy.register(buffer)
    buffer.allPositions().andReturn(List(
      AgentPosition("2000", "2000", None, format.parseDateTime("2014-01-01 08:00:00"), None),
      AgentPosition("2001", "2001", Some("52001"), format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 18:00:00"))))).anyTimes()

    whenExecuting(buffer) {
      AgentPositionProxy.agentForNumAndTime("2000", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual Some("2000")
      AgentPositionProxy.agentForNumAndTime("52001", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual Some("2001")
    }
  }

  it should "search the position in the database if it is not in the buffer" in {
    val buffer = mock[AgentPositionBuffer]
    AgentPositionProxy.register(buffer)
    buffer.allPositions().andReturn(List()).anyTimes()
    AgentPosition.insertOrUpdate(AgentPosition("2000", "2000", None, format.parseDateTime("2014-01-01 08:00:00"), None))

    whenExecuting(buffer) {
      AgentPositionProxy.agentForNumAndTime("2000", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual Some("2000")
      AgentPositionProxy.agentForNumAndTime("52001", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual None
    }
  }

  it should "search the position in the database if there is no buffer" in {
    AgentPositionProxy.positionBuffer = None
    AgentPosition.insertOrUpdate(AgentPosition("2000", "2000", None, format.parseDateTime("2014-01-01 08:00:00"), None))

    AgentPositionProxy.agentForNumAndTime("2000", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual Some("2000")
    AgentPositionProxy.agentForNumAndTime("52001", format.parseDateTime("2014-01-01 09:00:00")) shouldEqual None
  }

}
