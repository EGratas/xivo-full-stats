package xivo.fullstats.agent

import org.joda.time.Period
import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.model._
import xivo.fullstats.testutils.DBTest

class StatAgentManagerSpec extends DBTest(List("queue_log", "stat_agent_periodic", "agent_states")) with EasyMockSugar {

  // intervalle de 15 minutes (heures, minutes, secondes, millisecondes)
  val interval = new Period(0, 15, 0, 0)
  var manager: StatAgentManager = null

  override def beforeEach() {
    super.beforeEach()
    manager = new StatAgentManager(interval)
  }

  "A StatAgentManager" should "extract the agent number" in {
    manager.extractAgentNumber("Agent/123456") shouldEqual "123456"
  }

  it should "save the agent states and the periods" in {
    val computer = mock[StatAgentComputer]
    manager.agents.put("2000", computer)
    QueueLogTestUtils.insertQueueLog(QueueLog(1, format.parseDateTime("2014-01-01 08:40:00"), "123456.789", "test", "NONE", "ENTERQUEUE", None, None, None, None, None))
    computer.saveState(format.parseDateTime("2014-01-01 08:40:00"))

    whenExecuting(computer) {
      manager.saveState()
    }
  }

  it should "create a StatAgentComputer" in {
    val ql = QueueLog(1, format.parseDateTime("2014-01-01 08:01:00"), "123456.789", "NONE", "Agent/1000", "AGENTCALLBACKLOGIN", None, None, None, None, None)

    manager.parseEvent(ql)

    val c = manager.agents("1000")
    c.agent shouldEqual "1000"
    c.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:00"), State.LoggedOn)
    c.lastPeriod shouldEqual StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "1000", 0, 0, 0)
  }

  it should "notify the computers of a new interval" in {
    val qlDate = format.parseDateTime("2014-01-01 08:01:00")
    val ql = QueueLog(1, qlDate, "123456.789", "NONE", "Agent/1000", "ENTERQUEUE", None, None, None, None, None)
    val c1 = mock[StatAgentComputer]
    val c2 = mock[StatAgentComputer]
    manager.agents.put("1001", c1)
    manager.agents.put("1002", c2)
    manager.currentInterval = Some(format.parseDateTime("2014-01-01 07:45:00"))
    val sap = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 07:45:00"), "1001", 900, 10, 10)
    c1.notifyNewInterval(qlDate).andReturn(List(sap))
    c2.notifyNewInterval(qlDate).andReturn(List())

    whenExecuting(c1, c2) {
      manager.parseEvent(ql)
      manager.currentInterval shouldEqual Some(format.parseDateTime("2014-01-01 08:00:00"))
      StatAgentPeriodicTestUtils.allStatAgentPeriodic() shouldEqual List(sap)
    }
  }

  it should "switch to next period based on event time even if there is previous stored period" in {

    val statAgentPeriodicManager = mock[StatAgentPeriodicManager]
    val storedPeriod = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "1000", 0, 0, 0)
    val expectedPeriod = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "1000", 0, 0, 0)

    val c1 = mock[StatAgentComputer]
    manager.agents.put("1000", c1)

    expecting {
      call(statAgentPeriodicManager.lastForAgent("1000")).andReturn(Some(storedPeriod))
      call(statAgentPeriodicManager.insertAll(List()))
    }

    manager = new StatAgentManager(interval, statAgentPeriodicManager)

    val ql = QueueLog(1, format.parseDateTime("2014-01-01 08:31:00"), "123456.789", "NONE", "Agent/1000", "AGENTCALLBACKLOGIN", None, None, None, None, None)


    whenExecuting(statAgentPeriodicManager) {
      manager.parseEvent(ql)
      manager.currentInterval shouldEqual Some(format.parseDateTime("2014-01-01 08:30:00"))
    }
  }
}
