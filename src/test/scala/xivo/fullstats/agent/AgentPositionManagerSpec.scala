package xivo.fullstats.agent

import java.sql.Connection

import anorm._
import xivo.fullstats.model.{QueueLog, AgentPosition}
import xivo.fullstats.testutils.DBTest

import scala.util.Random

class AgentPositionManagerSpec extends DBTest(List("queue_log", "agent_position", "extensions", "dialaction")) {

  var manager: AgentPositionManager = null

  override def beforeEach(): Unit = {
    super.beforeEach()
    manager = new AgentPositionManager()
  }

  "An AgentPositionManager" should "save the ongoing positions" in {
    val position = AgentPosition("1000", "5000", None, formatMs.parseDateTime("2014-01-01 08:00:00.100"), None)
    manager.positions.append(position)

    manager.saveState()

    AgentPositionTestUtils.allAgentPositions() shouldEqual List(position)
  }

  it should "create a new position when receiving an AGENTCALLBACKLOGIN" in {
    val ql = QueueLog(0, formatMs.parseDateTime("2014-01-01 08:05:00.100"), "NONE", "NONE", "Agent/2000", "AGENTCALLBACKLOGIN", Some("1022"), None, None, None, None)
    manager.parseEvent(ql)

    manager.positions.map shouldEqual Map("2000" -> AgentPosition("2000", "1022", None, formatMs.parseDateTime("2014-01-01 08:05:00.100"), None))
  }

  it should "update the agent position taken from the buffer when receiving an AGENTCALLBACKLOGOFF" in {
    manager.positions.append(AgentPosition("2000", "1022", None, format.parseDateTime("2014-01-01 08:05:00"), None))
    val ql = QueueLog(0, formatMs.parseDateTime("2014-01-01 08:10:00.200"), "NONE", "NONE", "Agent/2000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    manager.parseEvent(ql)

    AgentPositionTestUtils.allAgentPositions() shouldEqual List(AgentPosition("2000", "1022", None, format.parseDateTime("2014-01-01 08:05:00"),
      Some(formatMs.parseDateTime("2014-01-01 08:10:00.200"))))
  }

  it should "update the agent position taken from the database when receiving an AGENTCALLBACKLOGOFF" in {
    AgentPosition.insertOrUpdate(AgentPosition("2000", "1022", None, format.parseDateTime("2014-01-01 08:05:00"), None))
    val ql = QueueLog(0, formatMs.parseDateTime("2014-01-01 08:10:00.200"), "NONE", "NONE", "Agent/2000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    manager.parseEvent(ql)

    AgentPositionTestUtils.allAgentPositions() shouldEqual List(AgentPosition("2000", "1022", None, format.parseDateTime("2014-01-01 08:05:00"),
      Some(formatMs.parseDateTime("2014-01-01 08:10:00.200"))))
  }

  "An AgentPositionBuffer" should "register itself to the AgentPositionProxy" in {
    val buffer = new AgentPositionBuffer()
    AgentPositionProxy.positionBuffer.get shouldBe theSameInstanceAs(buffer)
  }
}

object AgentPositionTestUtils {
  val random = new Random()

  def allAgentPositions()(implicit c: Connection): List[AgentPosition] =
    SQL("SELECT agent_num, line_number, sda, start_time, end_time FROM agent_position").as(AgentPosition.simple *)

  def createSdaForLine(sda: String, line: String)(implicit c: Connection): Unit = {
    val userId = random.nextInt()
    val incallId = random.nextInt()
    SQL("INSERT INTO dialaction(event, category, categoryval, action, actionarg1) VALUES ('answer', 'incall', {incallId}, 'user', {userId})")
      .on('userId -> userId, 'incallId -> incallId).executeUpdate()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({sda}, 'incall', {incallId})").on('incallId -> incallId, 'sda -> sda).executeUpdate()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({line}, 'user', {userId})").on('userId -> userId, 'line -> line).executeUpdate()
  }
}
