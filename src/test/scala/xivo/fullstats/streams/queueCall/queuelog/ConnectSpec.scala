package xivo.fullstats.streams.queueCall.queuelog

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep}
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest}

import scala.collection.mutable

class ConnectSpec extends DBTest(List("cel", "queue_log", "agentfeatures", "agent_position", "xc_queue_call")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val queueCallRepository = new QueueCallRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val answerTime = format.parseDateTime("2019-01-01 12:01:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val queueName = "queue1"

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val connect = new Connect
    val flowUnderTest = connect.flow

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = answerTime, callid = uniqueId,
      event = "CONNECT", queueName = queueName)

    val openedPosition = AgentPosition("20001", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 2001, "Agent", "One")

    def testStream(flowUnderTest: Flow[ConnectQl, QueueCall, NotUsed]) = {
      TestSource.probe[ConnectQl].via(flowUnderTest).toMat(TestSink.probe)(Keep.both).run()
    }
  }

  "Connect Flow" should "update answer time and ring duration" in new Helper {
    val connectQl = ql.copy(agent = "")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(answerTime = Some(answerTime), ringDuration = Some(60), termination = Some(QueueTerminationType.CONNECT))
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    val (source, sink) = testStream(flowUnderTest)

    sink.request(1)
    source.sendNext(ConnectQl(connectQl))
    sink.expectNext() shouldEqual expectedQueueCall

    queueCallRepository.repository shouldEqual expectedMap
  }

  it should "update answer time and ring duration from data2" in new Helper {
    val connectQl = ql.copy(agent = "", data2 = Some("987654.321"))

    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "987654.321", linkedId = "987654.111", eventType = "ANSWER",
      appName = "AppQueue", context = "agentcallback"))

    queueCallRepository.addCallToMap("987654.111", queueCall.copy(uniqueId = "987654.111"))

    val expectedQueueCall = queueCall.copy(uniqueId = "987654.111", answerTime = Some(answerTime), ringDuration = Some(60), termination = Some(QueueTerminationType.CONNECT))
    val expectedMap = mutable.HashMap("987654.111" -> expectedQueueCall)

    val (source, sink) = testStream(flowUnderTest)

    sink.request(1)
    source.sendNext(ConnectQl(connectQl))
    sink.expectNext() shouldEqual expectedQueueCall

    queueCallRepository.repository shouldEqual expectedMap
  }

  it should "update answer time and ring duration from database" in new Helper {
    val connectQl = ql.copy(agent = "")

    val insertedQueueCall = QueueCall.insert(queueCall.copy(uniqueId = uniqueId))

    val expectedQueueCall = queueCall.copy(id = insertedQueueCall.id, uniqueId = uniqueId, answerTime = Some(answerTime), ringDuration = Some(60), termination = Some(QueueTerminationType.CONNECT))
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    val (source, sink) = testStream(flowUnderTest)

    sink.request(1)
    source.sendNext(ConnectQl(connectQl))
    sink.expectNext() shouldEqual expectedQueueCall

    queueCallRepository.repository shouldEqual expectedMap
  }

  it should "update agent id " in new Helper {
    val connectQl = ql.copy(agent = "Agent/2001")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(answerTime = Some(answerTime), ringDuration = Some(60), agentId = Some(1L), termination = Some(QueueTerminationType.CONNECT))
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    val (source, sink) = testStream(flowUnderTest)

    sink.request(1)
    source.sendNext(ConnectQl(connectQl))
    sink.expectNext() shouldEqual expectedQueueCall

    queueCallRepository.repository shouldEqual expectedMap
  }
}
