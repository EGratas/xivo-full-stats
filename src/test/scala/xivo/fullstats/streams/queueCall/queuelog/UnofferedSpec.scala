package xivo.fullstats.streams.queueCall.queuelog

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class UnofferedSpec extends DBTest(List("queue_log")) {

  class Helper() {
    implicit val system: ActorSystem             = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val queueCallRepository = new QueueCallRepository

    val startTime   = format.parseDateTime("2019-01-01 12:00:00")
    val uniqueId    = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val queueName   = "queue1"

    val queueCall = CallTestUtils.createQueueCall(
      id = None,
      startTime = startTime,
      endTime = None,
      uniqueId = uniqueId
    )

    val unofferedGeneric = new Unoffered
    val flowUnderTest    = unofferedGeneric.flow

    val testSink                         = TestSink.probe[QueueCall]
    def streamUnderTest(ql: UnofferedQl) = Source(List(ql)).via(flowUnderTest)

    val ql: QueueLog = EmptyObjectProvider
      .emptyQueueLog()
      .copy(
        id = 1,
        time = startTime,
        callid = uniqueId,
        event = "CLOSED",
        queueName = queueName
      )

  }

  it should "create new queue call for CLOSED in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "CLOSED", time = startTime)

    val expectedQueueCall = queueCall.copy(
      queueRef = queueName,
      answerTime = None,
      endTime = Some(startTime),
      ringDuration = Some(0),
      termination = Some(QueueTerminationType.CLOSED),
      deleted = true
    )

    streamUnderTest(UnofferedQl(qlWithTermination))
      .runWith(testSink)
      .request(2)
      .expectNext(expectedQueueCall)
      .expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "create new queue call for FULL in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "FULL", time = startTime)

    val expectedQueueCall = queueCall.copy(
      queueRef = queueName,
      answerTime = None,
      endTime = Some(startTime),
      ringDuration = Some(0),
      termination = Some(QueueTerminationType.FULL),
      deleted = true
    )

    streamUnderTest(UnofferedQl(qlWithTermination))
      .runWith(testSink)
      .request(2)
      .expectNext(expectedQueueCall)
      .expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "create new queue call for JOINEMPTY in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "JOINEMPTY", time = startTime)

    val expectedQueueCall = queueCall.copy(
      queueRef = queueName,
      answerTime = None,
      endTime = Some(startTime),
      ringDuration = Some(0),
      termination = Some(QueueTerminationType.JOINEMPTY),
      deleted = true
    )

    streamUnderTest(UnofferedQl(qlWithTermination))
      .runWith(testSink)
      .request(2)
      .expectNext(expectedQueueCall)
      .expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "create new queue call for DIVERT_CA_RATIO in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "DIVERT_CA_RATIO", time = startTime)

    val expectedQueueCall = queueCall.copy(
      queueRef = queueName,
      answerTime = None,
      endTime = Some(startTime),
      ringDuration = Some(0),
      termination = Some(QueueTerminationType.DIVERT_CA_RATIO),
      deleted = true
    )

    streamUnderTest(UnofferedQl(qlWithTermination))
      .runWith(testSink)
      .request(2)
      .expectNext(expectedQueueCall)
      .expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "create new queue call for DIVERT_HOLDTIME in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "DIVERT_HOLDTIME", time = startTime)

    val expectedQueueCall = queueCall.copy(
      queueRef = queueName,
      answerTime = None,
      endTime = Some(startTime),
      ringDuration = Some(0),
      termination = Some(QueueTerminationType.DIVERT_HOLDTIME),
      deleted = true
    )

    streamUnderTest(UnofferedQl(qlWithTermination))
      .runWith(testSink)
      .request(2)
      .expectNext(expectedQueueCall)
      .expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }
}
