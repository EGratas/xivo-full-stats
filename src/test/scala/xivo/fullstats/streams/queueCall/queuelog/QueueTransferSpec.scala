package xivo.fullstats.streams.queueCall.queuelog

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class QueueTransferSpec extends DBTest(List("queue_log")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val queueCallRepository = new QueueCallRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val answerTime = format.parseDateTime("2019-01-01 12:03:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val queueName = "queue1"
    val agent = "Agent/2001"

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, answerTime = Some(answerTime))

    val queueTransfer = new QueueTransfer()
    val flowUnderTest = queueTransfer.flow

    val testSink = TestSink.probe[QueueCall]
    def streamUnderTest(ql: QueueTransferQl) = Source(List(ql)).via(flowUnderTest)

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = endTime, callid = uniqueId,
      event = "XIVO_QUEUE_TRANSFER", queueName = queueName, agent = agent, data1 = Some("1589964640.21"))
  }

  "QueueTransfer Flow" should "update transferred map in the repository" in new Helper {
    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(transferredCalls = Map("1589964640.21" -> uniqueId))
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(QueueTransferQl(ql)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository shouldEqual expectedMap
  }


}
