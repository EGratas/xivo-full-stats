package xivo.fullstats.streams.queueCall

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, FlowShape, Graph}
import akka.testkit.TestProbe
import org.scalatest.concurrent.Eventually
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallEvent, QueueCall, QueueLog}
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable
import scala.concurrent.duration._

class QueueCallGraphSpec extends DBTest(List("queue_log", "xc_queue_call")) with Eventually {
  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val repository: QueueCallRepository = new QueueCallRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val probe = TestProbe()

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(callid = uniqueId, event = "ENTERQUEUE", time = startTime, queueName = "queue1")
  }

  "Queue Call Graph" should "insert queue call" in new Helper {
    val celGraph = new QueueCallGraph
    val stream: Graph[FlowShape[CallEvent, QueueCall], NotUsed] = celGraph.graph

    val expectedQueueCall = queueCall.copy(id = Some(1), startTime = startTime, uniqueId = uniqueId, queueRef = "queue1")
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    val graphUnderTest = Source(List(ql)).viaMat(stream)(Keep.both).runWith(Sink.ignore)

    eventually(timeout(scaled(2.seconds))) {
      repository.repository(uniqueId).startTime shouldEqual startTime
      repository.repository(uniqueId).uniqueId shouldEqual uniqueId
      repository.repository(uniqueId).queueRef shouldEqual "queue1"
    }
  }
}
