package xivo.fullstats.streams.common

import akka.actor.{ActorSystem, Scheduler}
import akka.stream.ActorMaterializer
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.{TestActorRef, TestKit}
import anorm.SQL
import anorm.SqlParser.get
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.verify
import org.scalatest.concurrent.Eventually
import org.scalatest.mockito.MockitoSugar._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.streams.common.PeriodicCleanPendingCalls.{CleanCallChannels, CleanConversations, CleanQueueCalls, CleanTransfers}
import xivo.fullstats.streams.conversation.ConversationRepository
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.streams.transfer.TransferRepository
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBUtil}

import scala.concurrent.duration._

class PeriodicCleanPendingCallsSpec extends FlatSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with Eventually {

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  import materializer.executionContext

  implicit val connection = DBUtil.getConnection
  DBUtil.setupDB("database.xml")

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  override def beforeEach(): Unit = {
    DBUtil.cleanTables(List("cel", "xc_call_channel", "xc_call_conversation", "xc_call_transfer"))
  }

  class Helper() {
    val callChannelRepository = new CallChannelRepository
    val queueCallRepository = new QueueCallRepository
    val conversationRepository = new ConversationRepository
    val transferRepository = new TransferRepository

    val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val transferTimeCel = format.parseDateTime("2019-01-01 12:05:00")

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      emitted = false, uniqueId = "123456.789", termination = None, answerTime = Some(startTime))

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = "123456.789")

    def actor(scheduler: Scheduler = system.scheduler) = {
      val a = TestActorRef(new PeriodicCleanPendingCalls(callChannelRepository, queueCallRepository, conversationRepository, transferRepository, scheduler))
      (a, a.underlyingActor)
    }
  }

  "PeriodicCleanPendingCalls" should "clean call channel repository" in new Helper {
    val (ref, _) = actor()

    val keep = callChannel.copy(startTime = DateTime.now(), uniqueId = "123456.111")
    val delete = callChannel.copy(startTime = DateTime.now().minusHours(5), uniqueId = "123456.222")

    callChannelRepository.addCallToMap("123456.111", keep)
    callChannelRepository.addCallToMap("123456.222", delete)

    ref ! CleanCallChannels

    eventually(timeout(scaled(2.seconds))) {
      callChannelRepository.repository.isEmpty shouldBe false
      callChannelRepository.repository("123456.111") shouldEqual keep
      callChannelRepository.repository.get("123456.222") shouldBe None

      SQL("SELECT termination FROM xc_call_channel WHERE unique_id='123456.222' ").as(get[String]("termination").single) shouldEqual "system_hangup"
    }
  }

  it should "clean queue call repository" in new Helper {
    val (ref, _) = actor()
    ref ! CleanQueueCalls

    val pendingStartEndTime = DateTime.now().minusHours(5)

    val keep = queueCall.copy(startTime = DateTime.now(), uniqueId = "123456.111")
    val delete = queueCall.copy(startTime = pendingStartEndTime, uniqueId = "123456.222")

    queueCallRepository.addCallToMap("123456.111", keep)
    queueCallRepository.addCallToMap("123456.222", delete)

    val expected = delete.copy(endTime = Some(pendingStartEndTime), termination = Some(QueueTerminationType.SYSTEM_HANGUP))

    ref ! CleanQueueCalls

    eventually(timeout(scaled(2.seconds))) {
      queueCallRepository.repository.isEmpty shouldBe false
      queueCallRepository.repository("123456.111") shouldEqual keep
      queueCallRepository.repository.get("123456.222") shouldBe None

      val res = QueueCall.getByUniqueId("123456.222").get

      res.endTime.get shouldEqual pendingStartEndTime
      res.termination.get shouldEqual QueueTerminationType.SYSTEM_HANGUP
    }
  }

  it should "clean conversation repository" in new Helper {
    val (ref, a) = actor()
    val testSink = TestSink.probe[Conversation]

    val keep = callChannel.copy(startTime = DateTime.now(), uniqueId = "123456.111")
    val delete = callChannel.copy(startTime = DateTime.now().minusHours(5), uniqueId = "123456.222")

    val insertedKeep = CallChannel.insert(keep)
    val insertedDelete = CallChannel.insert(delete)
    val conversationLink = ConversationLinks("123456.222", List("123456.111"))

    conversationRepository.addToMap("123456.222", conversationLink)

    ref ! CleanConversations(List(delete))

    eventually(timeout(scaled(2.seconds))) {
      conversationRepository.repository.isEmpty shouldBe true
      val res = Conversation.getAll().head
      res.callerCallId shouldEqual insertedDelete.id.get
      res.calleeCallId shouldEqual insertedKeep.id.get
    }
  }

  it should "clean transfer repository" in new Helper {
    val (ref, _) = actor()

    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "123456.444", linkedId = "123456.333"))

    val channel1 = CallChannel.insert(callChannel.copy(id = Some(1), startTime = DateTime.now(), uniqueId = "123456.111"))
    val channel2 = CallChannel.insert(callChannel.copy(id = Some(1), startTime = DateTime.now(), uniqueId = "123456.222"))
    val channel3 = CallChannel.insert(callChannel.copy(id = Some(1), startTime = DateTime.now(), uniqueId = "123456.333"))
    val channel4 = CallChannel.insert(callChannel.copy(id = Some(1), startTime = DateTime.now(), uniqueId = "123456.444"))

    val transferLink = TransferLinks(linkedId = "123456.111",
      collectedUniqueIds = List("123456.111", "123456.222", "123456.333", "123456.444"),
      transferDecisionId = Some("123456.111"),
      onHold = Some("123456.222"),
      transferTime = Some(transferTimeCel)
    )

    transferRepository.addToMap("123456.111", List(transferLink))
    val expected = TransferCall(None, channel1.id.get, channel4.id.get, channel3.id.get, Some(channel2.id.get), transferTimeCel)

    ref ! CleanTransfers(List(channel1))

    eventually(timeout(scaled(2.seconds))) {
      transferRepository.repository.isEmpty shouldBe true
      TransferCall.getAll().size shouldEqual 1
      val res = TransferCall.getAll().head
      res.init shouldEqual channel1.id.get
      res.end  shouldEqual channel4.id.get
      res.hold shouldEqual channel3.id.get
      res.talk shouldEqual channel2.id
      res.transferTime shouldEqual transferTimeCel
    }
  }

  it should "schedule itself" in new Helper {
    val scheduler = mock[Scheduler]
    val (ref, _) = actor(scheduler)

    verify(scheduler).schedule(12.hours, 12.hours, ref, CleanCallChannels)
    verify(scheduler).schedule(12.hours, 12.hours, ref, CleanQueueCalls)
  }
}
