package xivo.fullstats.streams

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Source}
import akka.testkit.TestProbe
import org.scalatest.concurrent.Eventually
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.QueueLog
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.concurrent.duration._

class CallEventsProcessingGraphSpec extends DBTest(List("cel", "xc_call_channel", "xc_queue_call")) with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val answerTime = format.parseDateTime("2019-01-01 12:01:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      answerTime = Some(answerTime), emitted = false, uniqueId = uniqueId, termination = None)

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val probe = TestProbe()

    val chanStart = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_START")
    val chanEnd = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_END", context = "user")
    val enterQueue = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = startTime, callid = uniqueId,
      event = "ENTERQUEUE", queueName = "queue1")
    val completeAgent: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = endTime, callid = uniqueId,
      event = "COMPLETEAGENT", queueName = "queue1", agent = "Agent/2001")

    val callEventsGraph = new CallEventsProcessingGraph()
    val graphUnderTest = callEventsGraph.graph
    val callChannelRepository = callEventsGraph.callChannelRepository
    val queueCallRepository = callEventsGraph.queueCallRepository
  }

  "CallEvents Graph" should "insert call channel" in new Helper {
    Source(List(chanStart)).to(graphUnderTest).run()

    eventually(timeout(scaled(2.seconds))) {
      callChannelRepository.repository(uniqueId).channelInterface shouldEqual Some("SIP/larerx9a")
    }
  }

  it should "delete call channel" in new Helper {
    callChannelRepository.addCallToMap(uniqueId, callChannel)

    Source(List(chanEnd)).toMat(graphUnderTest)(Keep.right).run()

    eventually(timeout(scaled(2.seconds))) {
      callChannelRepository.repository.isEmpty shouldBe true
    }
  }

  it should "insert queue call" in new Helper {
    Source(List(enterQueue)).to(graphUnderTest).run()

    val expected = queueCall.copy(id = Some(1), queueRef = "queue1")

    eventually(timeout(scaled(2.seconds))) {
      queueCallRepository.repository.isEmpty shouldBe false
      val res = queueCallRepository.repository(uniqueId)
      res shouldEqual expected.copy(id = res.id)
    }
  }

  it should "delete queue call by acd call channel" in new Helper {
    queueCallRepository.addCallToMap(uniqueId, queueCall)

    Source(List(completeAgent)).toMat(graphUnderTest)(Keep.right).run()

    eventually(timeout(scaled(2.seconds))) {
      callChannelRepository.repository.isEmpty shouldBe true
      queueCallRepository.repository.isEmpty shouldBe true
    }
  }
}
