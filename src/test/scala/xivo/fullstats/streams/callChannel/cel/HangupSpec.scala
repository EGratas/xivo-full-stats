package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class HangupSpec extends DBTest(List("cel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val answerTime = format.parseDateTime("2019-01-01 12:00:30")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val remoteChannelName = "SIP/xpsm8mh9-00000007"
    val channelName = "SIP/larerx9a-00000008"
    val celExtraData = "{\"hangupcause\":16,\"hangupsource\":\"SIP/larerx9a-00000008\",\"dialstatus\":\"ANSWER\"}"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val hangup = new Hangup
    val flowUnderTest = hangup.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: HangupCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "HANGUP", extra = celExtraData)
  }

  "Hangup Flow" should "add end time and hangup" in new Helper {
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(endTime = Some(endTime), ringDuration = Some(300) ,termination = Some(CallTermination.Hangup))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(cel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "add end time and remote hangup" in new Helper {
    val remoteHangupCel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = remoteChannelName,
      uniqueId = uniqueId, eventType = "HANGUP", extra = "{\"hangupcause\":16,\"hangupsource\":\"SIP/larerx9a-00000008\",\"dialstatus\":\"ANSWER\"}")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(endTime = Some(endTime), ringDuration = Some(300), termination = Some(CallTermination.RemoteHangup))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(remoteHangupCel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "add end time and remote hangup on unknown hangup source" in new Helper {
    val remoteHangupCel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = remoteChannelName,
      uniqueId = uniqueId, eventType = "HANGUP", extra = "")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(endTime = Some(endTime), ringDuration = Some(300), termination = Some(CallTermination.RemoteHangup))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(remoteHangupCel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "add end time and remote hangup on invalid extra" in new Helper {
    val remoteHangupCel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = remoteChannelName,
      uniqueId = uniqueId, eventType = "HANGUP", extra = "this is not a valid json")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(endTime = Some(endTime), ringDuration = Some(300), termination = Some(CallTermination.RemoteHangup))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(remoteHangupCel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not process agentcallback event" in new Helper {
    val agentCallBackcel = cel.copy(context = "agentcallback")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(agentCallBackcel)).runWith(testSink).request(2).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not process transfer event" in new Helper {
    val transferCel = cel.copy(context = "transfer")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(transferCel)).runWith(testSink).request(2).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not update ring duration if already set" in new Helper {
    val callChannelAlreadyAnswered = CallTestUtils.createCallChannel(id = None, startTime = startTime,
      answerTime = Some(answerTime), ringDuration = Some(30), endTime = None, uniqueId = uniqueId,
      termination = None)
    celRepository.addCallToMap(uniqueId, callChannelAlreadyAnswered)

    val expectedCallChannel = callChannelAlreadyAnswered.copy(endTime = Some(endTime), ringDuration = Some(30), termination = Some(CallTermination.Hangup))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(cel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "update caller_num if not already set" in new Helper {
    val callChannelWithoutCallerNum = CallTestUtils.createCallChannel(id = None, startTime = startTime,
      answerTime = Some(answerTime), ringDuration = Some(30), endTime = None, uniqueId = uniqueId,
      termination = None)
    celRepository.addCallToMap(uniqueId, callChannelWithoutCallerNum)

    val expectedCallChannel = callChannelWithoutCallerNum.copy(endTime = Some(endTime),
      ringDuration = Some(30), termination = Some(CallTermination.Hangup), callerNum = Some("1000"))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(cel.copy(cidNum = "1000"))).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not update caller_num if caller_num is empty string" in new Helper {
    val callChannelWithoutCallerNum = CallTestUtils.createCallChannel(id = None, startTime = startTime,
      answerTime = Some(answerTime), ringDuration = Some(30), endTime = None, uniqueId = uniqueId,
      termination = None, callerNum = Some("1000"))
    celRepository.addCallToMap(uniqueId, callChannelWithoutCallerNum)

    val expectedCallChannel = callChannelWithoutCallerNum.copy(endTime = Some(endTime),
      ringDuration = Some(30), termination = Some(CallTermination.Hangup), callerNum = Some("1000"))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(cel.copy(cidNum = ""))).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not update caller_num if caller_num is set with another number" in new Helper {
    val callChannelWithoutCallerNum = CallTestUtils.createCallChannel(id = None, startTime = startTime,
      answerTime = Some(answerTime), ringDuration = Some(30), endTime = None, uniqueId = uniqueId,
      termination = None, callerNum = Some("1000"))
    celRepository.addCallToMap(uniqueId, callChannelWithoutCallerNum)

    val expectedCallChannel = callChannelWithoutCallerNum.copy(endTime = Some(endTime),
      ringDuration = Some(30), termination = Some(CallTermination.Hangup), callerNum = Some("1000"))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(HangupCel(cel.copy(cidNum = "3000"))).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }
}
