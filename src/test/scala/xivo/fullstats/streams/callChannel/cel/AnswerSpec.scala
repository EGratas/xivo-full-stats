package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class AnswerSpec extends DBTest(List("cel", "agentfeatures", "agent_position")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val answer = new Answer
    val flowUnderTest = answer.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: AnswerCel) = Source(List(cel)).via(flowUnderTest)
    def streamUnderTest(cel: List[AnswerCel]) = Source(cel).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "ANSWER")
  }

  "Answer Flow" should "answer on AppDial" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppDial"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer on AppQueue" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppQueue"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer on Answer" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "Answer"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer on Dial" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "Dial"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer acd and add agent id" in new Helper {
    val answerCelWithAgentId = AnswerCel(cel.copy(appName = "AppDial", cidNum = "2001"))

    val openedPosition = AgentPosition("8000", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 8000, "Agent", "One")
    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Acd))

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = Some(1L), callType = CallType.Acd)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCelWithAgentId).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer consultation" in new Helper {
    val answerCelWithAgentId = AnswerCel(cel.copy(appName = "AppDial", cidNum = "2001"))

    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Consultation))

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = None, callType = CallType.Consultation)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCelWithAgentId).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer queued" in new Helper {
    val answerCelWithAgentId = AnswerCel(cel.copy(appName = "AppDial", cidNum = "2001"))

    val openedPosition = AgentPosition("20001", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 2001, "Agent", "One")
    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Queued))

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = None, callType = CallType.Queued)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCelWithAgentId).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer administrative" in new Helper {
    val answerCelWithAgentId = AnswerCel(cel.copy(appName = "AppDial", cidNum = "2001"))

    val openedPosition = AgentPosition("20001", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 2001, "Agent", "One")
    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Administrative))

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = None, callType = CallType.Administrative)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCelWithAgentId).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer and add agent id by linkedid" in new Helper {
    val answerCelWithAgentId = AnswerCel(cel.copy(appName = "AppDial", cidNum = "2001", uniqueId = "987654.321", linkedId = uniqueId))

    val openedPosition = AgentPosition("8000", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 8000, "Agent", "One")
    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Acd))

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = Some(1L), callType = CallType.Acd)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCelWithAgentId).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer but not add non existing agent id" in new Helper {
    val malformedCel1 = AnswerCel(cel.copy(appName = "AppDial", cidNum = "1234"))
    val malformedCel2 = AnswerCel(cel.copy(appName = "AppDial", cidNum = "abcd"))

    val openedPosition = AgentPosition("8000", "2001", None, new DateTime(format.parseDateTime("2019-01-01 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(1, 1, 8000, "Agent", "One")
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300), agentId = None)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(List(malformedCel1, malformedCel2)).runWith(testSink).request(2)
      .expectNext(expectedCallChannel, expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer both current call and the queued call" in new Helper {
    val ongoingQueuedUniqueId = "987654.321"

    val answerCel = AnswerCel(cel.copy(appName = "AppDial", linkedId = ongoingQueuedUniqueId))

    val ongoingQueuedNotAnsweredCall = callChannel.copy(
      callType = CallType.Queued,
      answerTime = None,
      uniqueId = ongoingQueuedUniqueId
    )

    celRepository.addCallToMap(ongoingQueuedUniqueId, ongoingQueuedNotAnsweredCall)
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedQueuedCallChannel = ongoingQueuedNotAnsweredCall.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, ongoingQueuedUniqueId -> expectedQueuedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel, expectedQueuedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer both current call and the consultation call" in new Helper {
    val ongoingConsultationUniqueId = "987654.321"

    val answerCel = AnswerCel(cel.copy(appName = "AppDial", linkedId = ongoingConsultationUniqueId))

    val ongoingConsultationNotAnsweredCall = callChannel.copy(
      callType = CallType.Consultation,
      answerTime = None,
      uniqueId = ongoingConsultationUniqueId
    )

    celRepository.addCallToMap(ongoingConsultationUniqueId, ongoingConsultationNotAnsweredCall)
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedConsultationCallChannel = ongoingConsultationNotAnsweredCall.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, ongoingConsultationUniqueId -> expectedConsultationCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel, expectedConsultationCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "answer the consultation call for Local channels in user context" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "Dial", context = "user", chanName = "Local/123"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not answer in agent callback context" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppDial", context = "agentcallback"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(1).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not answer in xivo-pickup context" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppDial", context = "xivo-pickup"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap

  }

  it should "not answer the call for Local channels" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppDial", context = "xivo-pickup", chanName = "Local/123"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not answer call with end time" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "AppDial"))
    val callWithEndTime = callChannel.copy(endTime = Some(endTime))

    celRepository.addCallToMap(uniqueId, callWithEndTime)

    val expectedCallChannel = callWithEndTime.copy(answerTime = None)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not answer linked call with end time" in new Helper {
    val ongoingConsultationUniqueId = "987654.321"

    val answerCel = AnswerCel(cel.copy(appName = "AppDial", linkedId = ongoingConsultationUniqueId))

    val ongoingConsultationNotAnsweredCall = callChannel.copy(
      callType = CallType.Consultation,
      answerTime = None,
      uniqueId = ongoingConsultationUniqueId,
      endTime = Some(endTime)
    )

    celRepository.addCallToMap(ongoingConsultationUniqueId, ongoingConsultationNotAnsweredCall)
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(answerTime = Some(endTime), ringDuration = Some(300))
    val expectedConsultationCallChannel = ongoingConsultationNotAnsweredCall.copy(answerTime = None)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, ongoingConsultationUniqueId -> expectedConsultationCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not override ring duration or answer time if already set" in new Helper {
    val answerCel = AnswerCel(cel.copy(appName = "Answer"))

    val firstAnswerTime = format.parseDateTime("2019-01-01 12:00:20")

    celRepository.addCallToMap(uniqueId, callChannel.copy(
      answerTime = Some(firstAnswerTime),
      ringDuration = Some(20))
    )

    val expectedCallChannel = callChannel.copy(answerTime = Some(firstAnswerTime), ringDuration = Some(20))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(answerCel).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }
}
