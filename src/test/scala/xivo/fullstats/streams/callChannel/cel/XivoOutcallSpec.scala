package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.TestProbe
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest}

import scala.collection.mutable
import scala.concurrent.duration._

class XivoOutcallSpec extends DBTest(List("cel"))  {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = linkedId, uniqueId = uniqueId, eventType = "XIVO_OUTCALL")
    val celList: List[Cel] = CelTestUtils.eventsByAscendingId()

    val xivoOutcall = new XivoOutcall
    val flowUnderTest = xivoOutcall.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: XivoOutcallCel) = Source(List(cel)).via(flowUnderTest)
  }

  "XivoOutcall Flow" should "change the call direction to outgoing" in new Helper {

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedMap = mutable.HashMap(uniqueId -> callChannel.copy(emitted = true))
    val expectedCallChannel = callChannel.copy(emitted = true)

    streamUnderTest(XivoOutcallCel(cel.copy(eventType = "XIVO_INCALL"))).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set consultation while outcall when already in call (acd)" in new Helper {
    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), callType = CallType.Queued, answerTime = Some(new DateTime()))
    val insertedQueuedCallChannel = CallChannel.insert(queuedCallChannel)

    val userCallChannel = callChannel.copy(uniqueId = "232323.333", endTime = None, userId = Some(1), callType = CallType.Acd, answerTime = Some(new DateTime()))
    val insertedUserCallChannel = CallChannel.insert(userCallChannel)

    celRepository.addCallToMap("121212.222", insertedQueuedCallChannel)
    celRepository.addCallToMap("232323.333", insertedUserCallChannel)
    celRepository.addCallToMap(uniqueId, callChannel.copy(userId = Some(1)))

    val expectedCallChannel = callChannel.copy(emitted = true, callType = CallType.Consultation, userId = Some(1))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedQueuedCallChannel, "232323.333" -> insertedUserCallChannel)

    streamUnderTest(XivoOutcallCel(cel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set consultation while outcall when already in call (acd linkedid)" in new Helper {
    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), callType = CallType.Queued, answerTime = Some(new DateTime()))
    val insertedQueuedCallChannel = CallChannel.insert(queuedCallChannel)

    val userCallChannel = callChannel.copy(uniqueId = "232323.333", endTime = None, userId = Some(1), callType = CallType.Acd, answerTime = Some(new DateTime()))
    val insertedUserCallChannel = CallChannel.insert(userCallChannel)

    celRepository.addCallToMap("121212.222", insertedQueuedCallChannel)
    celRepository.addCallToMap("232323.333", insertedUserCallChannel)
    celRepository.addCallToMap(linkedId, callChannel.copy(uniqueId = linkedId, userId = Some(1)))

    val expectedCallChannel = callChannel.copy(uniqueId = linkedId, emitted = true, callType = CallType.Consultation, userId = Some(1))
    val expectedMap = mutable.HashMap(linkedId -> expectedCallChannel, "121212.222"-> insertedQueuedCallChannel, "232323.333" -> insertedUserCallChannel)

    streamUnderTest(XivoOutcallCel(cel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }
}
