package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class ChanEndSpec extends DBTest(List("cel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val chanEnd = new ChanEnd
    val flowUnderTest = chanEnd.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: ChanEndCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_END")
  }

  "ChanEnd Flow" should "delete the call from call repository" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "user"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = true)
    val expectedMap = mutable.HashMap()

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "delete the call from call repository in AppDial" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "queue", appName = "AppDial"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = true)
    val expectedMap = mutable.HashMap()

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "delete the call from call repository in AppQueue" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "", appName = "AppQueue"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = true)
    val expectedMap = mutable.HashMap()

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "delete the *XXXX call from call repository" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "default", appName = "", exten = "*82002"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = true)
    val expectedMap = mutable.HashMap()

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "delete the DAHDI call from call repository" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "default", chanName = "DAHDI/123"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = true)
    val expectedMap = mutable.HashMap()

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not delete the call in not allowed context" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "some-context"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = false)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not delete the call in not allowed context and app" in new Helper {
    val chanEndCel = ChanEndCel(cel.copy(context = "some-context", appName = "some-app"))

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(deleted = false)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(chanEndCel).runWith(testSink).request(1).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }
}
