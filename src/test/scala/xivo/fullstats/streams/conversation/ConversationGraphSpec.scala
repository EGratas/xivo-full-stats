package xivo.fullstats.streams.conversation

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Keep
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import akka.stream.{ActorMaterializer, FlowShape, Graph}
import akka.testkit.TestProbe
import org.joda.time.DateTime
import org.scalatest.concurrent.Eventually
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

class ConversationGraphSpec extends DBTest(List("cel", "xc_call_conversation")) with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val conversationRepository = new ConversationRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_END")

    def testStream(graphUnderTest: Graph[FlowShape[CallEvent, Conversation], NotUsed]) = {
      TestSource.probe[Cel].via(graphUnderTest).toMat(TestSink.probe)(Keep.both).run()
    }
  }

  "Conversation Graph" should "insert conversation" in new Helper {
    val conversationGraph = new ConversationGraph
    val stream: Graph[FlowShape[CallEvent, Conversation], NotUsed] = conversationGraph.graph

    val callChannel1 = callChannel.copy(id = Some(1), uniqueId = "123456.789", answerTime = Some(new DateTime()))
    val callChannel2 = callChannel.copy(id = Some(2), uniqueId = "987654.321", answerTime = Some(new DateTime()))

    val chanEndCel = cel.copy(uniqueId = "987654.321", linkedId = "123456.789", appName = "AppDial")

    val insertedCaller = CallChannel.insert(callChannel1)
    val insertedCallee = CallChannel.insert(callChannel2)

    val conversationLinks = ConversationLinks("123456.789", List("987654.321"))
    conversationRepository.addToMap("123456.789", conversationLinks)

    val expectedConversation = Conversation(None, insertedCaller.id.get, insertedCallee.id.get)

    val (source, sink) = testStream(stream)

    sink.request(1)
    source.sendNext(chanEndCel)
    sink.expectNext() shouldEqual expectedConversation
  }

  it should "add conversation links in repository conversation" in new Helper {
    val conversationGraph = new ConversationGraph
    val stream: Graph[FlowShape[CallEvent, Conversation], NotUsed] = conversationGraph.graph

    val bridgeEnterCel = cel.copy(uniqueId = "987654.321", linkedId = "123456.789", eventType = "BRIDGE_ENTER")

    val expectedConversationLinks = ConversationLinks("123456.789", List("987654.321"))
    val expectedMap = Map("123456.789" -> expectedConversationLinks)

    val (source, sink) = testStream(stream)

    sink.request(1)
    source.sendNext(bridgeEnterCel)
    source.sendComplete()
    sink.expectComplete()
    conversationRepository.repository shouldEqual expectedMap
  }
}
