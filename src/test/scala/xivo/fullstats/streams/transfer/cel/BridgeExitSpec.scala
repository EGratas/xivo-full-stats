package xivo.fullstats.streams.transfer.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{BridgeExitCel, TransferLinks}
import xivo.fullstats.streams.transfer.TransferRepository
import xivo.fullstats.testutils.DBTest

class BridgeExitSpec  extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val transferRepository = new TransferRepository

    val endTimeCaller = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val bridgeExit = new BridgeExit
    val flowUnderTest = bridgeExit.flow

    val testSink = TestSink.probe[List[TransferLinks]]

    def streamUnderTest(cel: BridgeExitCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTimeCaller, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "BRIDGE_EXIT", context = "default", appName = "AppDial")

    val transferLink = TransferLinks(linkedId = linkedId, collectedUniqueIds = List(), transferDecisionId = None,
      onHold = None, transferTime = None, doSaveToDatabase = false, stopCollecting = false)
  }

  "BridgeEnter Flow" should "add first linked call channel" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val expected = List(transferLink.copy(collectedUniqueIds = List(uniqueId)))

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "add second linked call channel" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = "111111.222")
    val existingTransferLink = transferLink.copy(collectedUniqueIds = List(uniqueId))

    transferRepository.addToMap(linkedId, List(existingTransferLink))

    val expected = List(transferLink.copy(collectedUniqueIds = List("111111.222", uniqueId)))

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "ignore agentcallback context" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "agentcallback")

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore transfer context" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "transfer")

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_wait context" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "xuc_attended_xfer_wait")

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_end context" in new Helper {
    val bridgeExitCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "xuc_attended_xfer_end")

    streamUnderTest(BridgeExitCel(bridgeExitCel)).runWith(testSink).request(1).expectComplete()
  }
}
