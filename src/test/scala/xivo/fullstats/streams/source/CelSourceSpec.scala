package xivo.fullstats.streams.source

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.TestKit
import anorm._
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterAll
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model.{CallChannel, CallTermination, Cel, Conversation, TransferCall}
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest, DBUtil}

class CelSourceSpec extends DBTest(List("cel", "xc_call_channel", "xc_queue_call", "xc_call_conversation", "xc_call_transfer")) with BeforeAndAfterAll {

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper() {
    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val closingHolder = new ClosingHolder

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      emitted = false, uniqueId = uniqueId, termination = None)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId,
      uniqueId = uniqueId, eventType = "CHAN_START")

    val testSink = TestSink.probe[Cel]

    def insertCallChannels(callChannels: List[CallChannel]) : List[Long] = {
      callChannels.map(CallChannel.insert(_).id.getOrElse(-1L))
    }

    def insertConversation(channelIds: List[Long]) : Long = {
      val conversation = Conversation(None, channelIds.head, channelIds.last)
      Conversation.insert(conversation).id.getOrElse(-1L)
    }

    def insertTransfer(channelIds: List[Long]) : Long = {
      val transfer = TransferCall(None, channelIds.head, channelIds(1),
        channelIds(2), Some(channelIds.last), new DateTime())
      TransferCall.insert(transfer).id.getOrElse(-1L)
    }

    def getConversationById(id: Long): List[Conversation] =
      SQL("SELECT * FROM xc_call_conversation WHERE id={id}").on('id -> id).as(Conversation.simple *)

    def getTransferById(id: Long): List[TransferCall] =
      SQL("SELECT * FROM xc_call_transfer WHERE id={id}").on('id -> id).as(TransferCall.simple *)
  }

  "CelSourceSpec" should "delete pending call channels" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (_ <- 1 to 5) yield CallChannel.insertOrUpdate(callChannel)).toList }

    var res = CallChannel.getPendingUniqueIds(startTime)

    res shouldEqual  insertedCallChannels.map(_.uniqueId)
    res.size shouldEqual 5

    val celSource = new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)
    res = CallChannel.getPendingUniqueIds(startTime)

    res.isEmpty shouldBe true
    res.size shouldEqual 0
  }

  it should "delete pending call channels only within call thread" in new Helper {
    val callChannel1 = CallChannel.insertOrUpdate(callChannel.copy(uniqueId = "123456.111", startTime = startTime.minusMinutes(2), endTime = Some(endTime.minusMinutes(2))))
    val callChannel2 = CallChannel.insertOrUpdate(callChannel.copy(uniqueId = "123456.222", startTime = startTime.minusMinutes(1), endTime = Some(endTime.minusMinutes(1))))
    val callChannel3 = CallChannel.insertOrUpdate(callChannel.copy(uniqueId = "123456.333", startTime = startTime.plusMinutes(1)))
    val callChannel4 = CallChannel.insertOrUpdate(callChannel.copy(uniqueId = "123456.444", startTime = startTime.plusMinutes(2), endTime = Some(endTime.plusMinutes(2)), originalCallId = callChannel1.id))

    val pendingStartTime = CallChannel.getPendingStartTime().get
    val res = CallChannel.getAllPendingUniqueIds(CallChannel.getPendingUniqueIds(pendingStartTime))

    res shouldEqual List("123456.333", "123456.444", "123456.111")
  }

  it should "set stale pending call channels as stalled" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (i <- 1 to 5) yield CallChannel.insertOrUpdate(callChannel.copy(uniqueId = uniqueId + i))).toList }
    val insertedMostRecentCallChannel: CallChannel = CallChannel.insertOrUpdate(callChannel.copy(startTime = startTime.plusDays(5), endTime = None))

    CallChannel.setStalePendingCalls()

    CallChannel.getPendingUniqueIds(startTime).size shouldEqual 6
    CallChannel.getByUniqueId(uniqueId).get shouldEqual insertedMostRecentCallChannel
    val res = for (i <- 1 to 5) yield CallChannel.getByUniqueId(uniqueId + i).get
    res shouldEqual insertedCallChannels.map(c => c.copy(endTime = Some(startTime), termination = Some(CallTermination.Stalled)))
  }

  it should "delete pending conversation on pending caller channel" in new Helper {
    val insertedChannels = insertCallChannels(
      List(
        callChannel.copy(uniqueId = "123456.789"),
        callChannel.copy(uniqueId = "987654.321"))
    )

    val insertedConversationId = insertConversation(insertedChannels)
    getConversationById(insertedConversationId).size shouldEqual 1
    new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)
    getConversationById(insertedConversationId).size shouldEqual 0
  }

  it should "delete pending conversation on pending callee channel" in new Helper {
    val insertedChannels = insertCallChannels(
      List(
        callChannel.copy(uniqueId = "123456.789", endTime = Some(new DateTime())),
        callChannel.copy(uniqueId = "987654.321")
      )
    )

    val insertedConversationId = insertConversation(insertedChannels)
    getConversationById(insertedConversationId).size shouldEqual 1
    new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)
    getConversationById(insertedConversationId).size shouldEqual 0
  }

  it should "delete pending transfer on pending init channel" in new Helper {
    val insertedChannels = insertCallChannels(
      List(
       callChannel.copy(uniqueId = "1111.111"),
       callChannel.copy(uniqueId = "1111.222", endTime = Some(new DateTime())),
       callChannel.copy(uniqueId = "1111.333", endTime = Some(new DateTime())),
       callChannel.copy(uniqueId = "1111.444", endTime = Some(new DateTime())),
      )
    )

    val insertedTransferId = insertTransfer(insertedChannels)
    getTransferById(insertedTransferId).size shouldEqual 1
    new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)
    getTransferById(insertedTransferId).size shouldEqual 0
  }

  it should "emit cels" in new Helper {
    val toInsertCels: List[Cel] = (for (i <- 1 to 5) yield cel.copy(id = i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val celSource = new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)

    celSource.celsSource.runWith(testSink).request(5).expectNextN(toInsertCels)
  }

  it should "emit pending cels" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (_ <- 1 to 5) yield CallChannel.insertOrUpdate(callChannel)).toList }

    val toInsertCels: List[Cel] = (for (i <- 1 to 5) yield cel.copy(id = i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val celSource = new CelSource(999, closingHolder, Some(startTime), DBUtil.getConnection)

    celSource.pendingCelsSource.runWith(testSink).request(6).expectNextN(toInsertCels).expectComplete()
  }

  it should "emit no pending cels" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (_ <- 1 to 5) yield CallChannel.insertOrUpdate(callChannel)).toList }

    val toInsertCels: List[Cel] = (for (i <- 1 to 5) yield cel.copy(id = i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val celSource = new CelSource(999, closingHolder, Some(startTime.plusHours(1)), DBUtil.getConnection)

    celSource.pendingCelsSource.runWith(testSink).request(1).expectComplete()
  }

  it should "emit no stalled cels" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (i <- 1 to 5) yield {
        CallChannel.insertOrUpdate(callChannel.copy(uniqueId = uniqueId+i, startTime = startTime.plusHours(i)))
      }).toList }
    val insertedStalledCallChannel = CallChannel.insert(callChannel.copy(uniqueId = uniqueId+6, startTime = startTime.minusDays(2)))

    val startFrom = CallChannel.getPendingStartTime()

    val toInsertCels: List[Cel] = (for (i <- 1 to 6) yield cel.copy(id = i, uniqueId = uniqueId+i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val celSource = new CelSource(999, closingHolder, startFrom, DBUtil.getConnection)

    celSource.pendingCelsSource.runWith(testSink).request(6).expectNextN(toInsertCels.dropRight(1)).expectComplete()
  }

  it should "emit cels together with pending cel" in new Helper {
    val insertedCallChannels: List[CallChannel] = {
      (for (_ <- 1 to 5) yield CallChannel.insertOrUpdate(callChannel)).toList }

    val toInsertPendingCels: List[Cel] = (for (i <- 1 to 5) yield cel.copy(id = i)).toList
    toInsertPendingCels.foreach(CelTestUtils.insertCel)

    val toInsertCels: List[Cel] = (for (i <- 6 to 10) yield cel.copy(id = i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val celSource = new CelSource(2, closingHolder, Some(startTime), DBUtil.getConnection)

    celSource.source.runWith(testSink).request(10).expectNextN(toInsertPendingCels ++ toInsertCels)
  }

  it should "complete" in new Helper {
    val celSource = new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)

    closingHolder.close()

    celSource.source.runWith(testSink).expectSubscriptionAndComplete()
  }

  it should "protect against infinite loop" in new Helper {
    val inserted1 = CallChannel.insert(callChannel.copy(startTime = startTime))
    val inserted2 = CallChannel.insert(callChannel.copy(startTime = startTime))

    CallChannel.update(inserted1.copy(originalCallId = inserted2.id))
    CallChannel.update(inserted2.copy(originalCallId = inserted1.id))

    val celSource = new CelSource(0, closingHolder, Some(startTime), DBUtil.getConnection)

    celSource.pendingCelsSource.runWith(testSink).expectSubscriptionAndComplete()
  }
}
