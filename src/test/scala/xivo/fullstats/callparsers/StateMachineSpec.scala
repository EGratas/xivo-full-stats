package xivo.fullstats.callparsers

import java.sql.Connection

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{Matchers, FlatSpec}
import xivo.fullstats.model.{CallSummaryTable, CallSummary, CallEvent}


class StateMachineSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A StateMachine" should "transfer the event to the state, modify the current state and save the result" in {
    val oldState = mock[State[CallEvent, CallSummary]]
    val newState = mock[State[CallEvent, CallSummary]]
    val table = mock[CallSummaryTable[CallSummary]]
    implicit val c = mock[Connection]

    val myMachine = new StateMachine[CallEvent, CallSummary](table) {
      override def getInitialState: State[CallEvent, CallSummary] = oldState
      override def forceCloture(): Unit = ???
    }

    val event = mock[CallEvent]
    val summary = mock[CallSummary]
    val insertedSummary = mock[CallSummary]
    oldState.processEvent(event).andReturn(newState)
    newState.getResult.andReturn(summary)
    table.insertOrUpdate(summary).andReturn(insertedSummary)
    newState.setResult(insertedSummary)

    whenExecuting(oldState, newState, table) {
      myMachine.processEvent(event)
      myMachine.state shouldBe theSameInstanceAs(newState)
    }
  }

  it should "not save the result if the state does not change" in {
    val theState = mock[State[CallEvent, CallSummary]]
    val table = mock[CallSummaryTable[CallSummary]]
    implicit val c = mock[Connection]

    val myMachine = new StateMachine[CallEvent, CallSummary](table) {
      override def getInitialState: State[CallEvent, CallSummary] = theState
      override def forceCloture(): Unit = ???
    }

    val event = mock[CallEvent]
    theState.processEvent(event).andReturn(theState)

    whenExecuting(theState, table) {
      myMachine.processEvent(event)
      myMachine.state shouldBe theSameInstanceAs(theState)
    }
  }

  it should "stop processing the events after an exception occurs, and return None for the result" in {
    val theState = mock[State[CallEvent, CallSummary]]
    val table = mock[CallSummaryTable[CallSummary]]
    implicit val c = mock[Connection]

    val myMachine = new StateMachine[CallEvent, CallSummary](table) {
      override def getInitialState: State[CallEvent, CallSummary] = theState
      override def forceCloture(): Unit = ???
    }

    val event1 = mock[CallEvent]
    val event2 = mock[CallEvent]
    theState.processEvent(event1).andThrow(new RuntimeException)
    theState.getResult.andReturn(mock[CallSummary])

    whenExecuting(theState, table) {
      myMachine.processEvent(event1)
      myMachine.processEvent(event2)
      myMachine.getResult shouldEqual None
    }
  }
}
