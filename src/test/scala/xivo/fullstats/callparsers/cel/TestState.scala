package xivo.fullstats.callparsers.cel

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallData, Cel}

class TestState extends FlatSpec with Matchers with BeforeAndAfterEach {

  protected var cel: Cel = null
  protected var callData: CallData = null

  override def beforeEach() = {
    cel = EmptyObjectProvider.emptyCel()
    callData = EmptyObjectProvider.emptyCallData()
    cel.linkedId = "123456.789"
    callData.uniqueId = "123456.789"
  }

  private class AnonymousState(callData: CallData) extends CelState(callData: CallData, false) {
    override def processCel(cel: Cel) = this
  }

  "Any state" should "parse the ATTACHED_DATA" in {
    val cel = EmptyObjectProvider.emptyCel()
    cel.eventType = "ATTACHED_DATA"
    cel.appData = "ATTACHED_DATA,myKey=myValue"

    val state = new AnonymousState(EmptyObjectProvider.emptyCallData())
    state.processEvent(cel)

    state.getResult.attachedData.size shouldEqual 1
    val data = state.getResult.attachedData.head
    data.key shouldEqual "myKey"
    data.value shouldEqual "myValue"
  }

  it should "not fail with not properly formatted userfield" in {
    val cel = EmptyObjectProvider.emptyCel()
    cel.eventType = "ATTACHED_DATA"
    cel.appData = "ATTACHED_DATA"

    val state = new AnonymousState(EmptyObjectProvider.emptyCallData())
    state.processEvent(cel)

    state.getResult.attachedData.size shouldEqual 0

    cel.appData = "ATTACHED_DATA,myKey:myValue"

    state.processEvent(cel)

    state.getResult.attachedData.size shouldEqual 0
  }
}