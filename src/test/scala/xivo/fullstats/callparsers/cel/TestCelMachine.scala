package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{HoldPeriod, CallDirection, CallData}
import xivo.fullstats.testutils.DBTest

class TestCelMachine extends DBTest(List("call_data", "call_element")) with EasyMockSugar {

  var channelMgr: ChannelManager = null
  var celMachine: CelMachine = null

  override def beforeEach(): Unit = {
    super.beforeEach()
    celMachine = new CelMachine()
    channelMgr = mock[ChannelManager]
    celMachine.state = mock[CelState]
  }

  "A CelMachine" should "create a ChannelManager when the call data id is defined" in {
    celMachine.channelMgr shouldEqual None

    val cd1 = CallData(None, "123456.789", None, CallDirection.Incoming, new DateTime(), None, None, None, None, false, None, None)
    val cd2 = cd1.copy(id=Some(5))
    val cel  = EmptyObjectProvider.emptyCel()
    celMachine.state.processEvent(cel).andReturn(celMachine.state)
    celMachine.state.getResult.andReturn(cd1)
    celMachine.state.processEvent(cel).andReturn(celMachine.state)
    celMachine.state.getResult.andReturn(cd2)

    whenExecuting(celMachine.state) {
      celMachine.processEvent(cel)
      celMachine.channelMgr.isEmpty shouldBe true

      celMachine.processEvent(cel)
      celMachine.channelMgr.isEmpty shouldBe false
      celMachine.channelMgr.get.callDataId shouldEqual 5
    }
  }

  it should "forward the cel to the channel manager" in {
    celMachine.channelMgr = Some(channelMgr)
    val cel = EmptyObjectProvider.emptyCel()
    celMachine.state.processEvent(cel).andReturn(celMachine.state)
    channelMgr.processEvent(cel)

    whenExecuting(celMachine.state, channelMgr) {
      celMachine.processEvent(cel)
    }
  }

  "CelMachine.forceCloture" should "set the end time equal to answer time if defined, and forward to the ChannelManager" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.startTime = format.parseDateTime("2014-01-01 08:00:00")
    cd.answerTime = Some(format.parseDateTime("2014-01-01 08:01:00"))
    celMachine.state.getResult.andReturn(cd)
    channelMgr.forceCloture()
    celMachine.channelMgr = Some(channelMgr)

    whenExecuting(celMachine.state, channelMgr) {
      celMachine.forceCloture()
      cd.endTime shouldEqual cd.answerTime
    }
  }

  it should "set the end time equal to start time if answer time not defined" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.startTime = format.parseDateTime("2014-01-01 08:00:00")
    celMachine.state.getResult.andReturn(cd)

    whenExecuting(celMachine.state) {
      celMachine.forceCloture()
      cd.endTime shouldEqual Some(cd.startTime)
    }
  }

  it should "do nothing if the end date is set" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.endTime = Some(format.parseDateTime("2015-01-01 08:10:00"))
    cd.startTime = format.parseDateTime("2015-01-01 08:00:00")
    celMachine.state.getResult.andReturn(cd)

    whenExecuting(celMachine.state) {
      celMachine.forceCloture()
      cd.endTime shouldEqual Some(format.parseDateTime("2015-01-01 08:10:00"))
    }
  }

  it should "close the hold periods" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.startTime = format.parseDateTime("2014-01-01 08:00:00")
    cd.holdPeriods = List(
      HoldPeriod(format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 08:01:00")), "12346.789"),
      HoldPeriod(format.parseDateTime("2014-01-01 08:02:00"), None, "12346.789")
    )
    celMachine.state.getResult.andReturn(cd)

    whenExecuting(celMachine.state) {
      celMachine.forceCloture()
      cd.endTime shouldEqual Some(cd.startTime)
      cd.holdPeriods shouldEqual List(
        HoldPeriod(format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 08:01:00")), "12346.789"),
        HoldPeriod(format.parseDateTime("2014-01-01 08:02:00"), Some(format.parseDateTime("2014-01-01 08:02:00")), "12346.789")
      )
    }
  }

}
