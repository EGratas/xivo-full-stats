package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallElementTestUtils, CallElement, CallData, CallDirection}
import xivo.fullstats.testutils.DBTest

class TestChannelMachine extends DBTest(List("call_element")) {

  val cd = CallData.insert(CallData(None, "12345.789", None, CallDirection.Incoming, new DateTime(), None, None, None, None, false, None, None))

  "A ChannelMachine" should "recognize cels based on the channel name" in {
    val sip = "SIP/jhfaa"
    val cel = EmptyObjectProvider.emptyCel().copy(chanName = s"$sip-1278;2")

    val machine = new ChannelMachine(sip, cd.id.get, new DateTime)

    machine.isEventForMe(cel) shouldBe true
    machine.isEventForMe(cel.copy(chanName="Local/456456-45416")) shouldBe false
  }

  it should "rejects CELs if uniqueId is not None and does not match the one in the CEL" in {
    val sip = "SIP/jhfaa"
    val cel = EmptyObjectProvider.emptyCel().copy(chanName = s"$sip-1278;2", uniqueId = "999.99")

    val machine = new ChannelMachine(sip, cd.id.get, new DateTime)
    machine.uniqueId = Some("111.11")

    machine.isEventForMe(cel) shouldBe false
  }

  it should "accept CELs if uniqueId is not None and matches the one in the CEL" in {
    val sip = "SIP/jhfaa"
    val cel = EmptyObjectProvider.emptyCel().copy(chanName = s"$sip-1278;2", uniqueId = "111.11")

    val machine = new ChannelMachine(sip, cd.id.get, new DateTime)
    machine.uniqueId = Some("111.11")

    machine.isEventForMe(cel) shouldBe true
  }

  it should "set the answer time when receiving an event ANSWER with application AppDial" in {
    val d1 = new DateTime()
    val d2 = d1.plus(1000)
    val interface = "SIP/aegja"
    val cel = EmptyObjectProvider.emptyCel().copy(eventTime = d2, eventType = "ANSWER", appName = "AppDial", chanName = s"$interface-1245678")
    val machine = new ChannelMachine(interface, cd.id.get, d1)

    machine.processEvent(cel)

    val expectedElement = CallElement(None, cd.id.get, d1, Some(d2), None, interface, None)
    machine.callElement.copy(id=None) shouldEqual expectedElement
    CallElementTestUtils.allCallElements.map(_.copy(id=None)) shouldEqual List(expectedElement)
  }

  it should "set the end time and hangup the channel when receiving an event CHAN_END" in {
    val d1 = new DateTime()
    val d2 = d1.plus(1000)
    val interface = "SIP/aegja"
    val cel = EmptyObjectProvider.emptyCel().copy(eventTime = d2, eventType = "CHAN_END", chanName = s"$interface-1245678")
    val machine = new ChannelMachine(interface, cd.id.get, d1)

    machine.processEvent(cel)

    val expectedElement = CallElement(None, cd.id.get, d1, None, Some(d2), interface, None)
    machine.callElement.copy(id=None) shouldEqual expectedElement
    CallElementTestUtils.allCallElements.map(_.copy(id=None)) shouldEqual List(expectedElement)
    machine.isChannelHangedUp shouldBe true
  }

  it should "set the end time and hangup the channel when receiving an event LINKEDID_END" in {
    val d1 = new DateTime()
    val d2 = d1.plus(1000)
    val interface = "SIP/aegja"
    val cel = EmptyObjectProvider.emptyCel().copy(eventTime = d2, eventType = "LINKEDID_END", chanName = s"$interface-1245678")
    val machine = new ChannelMachine(interface, cd.id.get, d1)

    machine.processEvent(cel)

    val expectedElement = CallElement(None, cd.id.get, d1, None, Some(d2), interface, None)
    machine.callElement.copy(id=None) shouldEqual expectedElement
    CallElementTestUtils.allCallElements.map(_.copy(id=None)) shouldEqual List(expectedElement)
    machine.isChannelHangedUp shouldBe true
  }

  it should "update its internal uniqueid when receiving an event CHAN_START" in {
    val d1 = new DateTime()
    val d2 = d1.plus(1000)
    val interface = "SIP/aegja"
    val uId = "12356.44"
    val cel = EmptyObjectProvider.emptyCel().copy(eventTime = d2, uniqueId = uId, eventType = "CHAN_START", chanName = s"$interface-1245678")
    val machine = new ChannelMachine(interface, cd.id.get, d1)

    machine.processEvent(cel)

    machine.uniqueId shouldEqual Some(uId)
  }

  "ChannelMachine.forceCloture" should "set the end time equal to answer time if defined, and forward to the ChannelManager" in {
    val d = new DateTime
    val machine = new ChannelMachine("SIP/123", cd.id.get, d)

    machine.forceCloture()

    CallElementTestUtils.allCallElements.map(_.copy(id=None)) shouldEqual List(CallElement(None, cd.id.get, d, None, Some(d), "SIP/123", None))
  }

  it should "set the end time equal to start time if answer time not defined" in {
    val start = new DateTime
    val answer = start.plusMinutes(2)
    val machine = new ChannelMachine("SIP/123", cd.id.get, start)
    machine.callElement = machine.callElement.copy(answerTime = Some(answer))

    machine.forceCloture()

    CallElementTestUtils.allCallElements.map(_.copy(id=None)) shouldEqual List(CallElement(None, cd.id.get, start, Some(answer), Some(answer), "SIP/123", None))
  }

  it should "do nothing if the end date is set" in {
    val start = new DateTime
    val end = start.plusMinutes(2)
    val machine = new ChannelMachine("SIP/123", cd.id.get, start)
    machine.callElement = machine.callElement.copy(endTime = Some(end))

    machine.forceCloture()

    machine.callElement.copy(id=None) shouldEqual CallElement(None, cd.id.get, start, None, Some(end), "SIP/123", None)
  }
}
