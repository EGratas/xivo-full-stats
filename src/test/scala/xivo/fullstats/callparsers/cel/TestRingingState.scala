package xivo.fullstats.callparsers.cel

import org.joda.time.format.DateTimeFormat

class TestRingingState extends TestState {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  "The Ringing state" should "remember ring start time" in {
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")

    val answered = new Ringing(callData, cel)

    answered.ringStartTime shouldEqual format.parseDateTime("2014-05-23 14:05:23")
  }
  
  it should "return Answered and set ring duration on ANSWER with application AppDial" in {
    val ringing = new Ringing(callData, cel)
    ringing.ringStartTime = format.parseDateTime("2014-05-23 14:05:23")
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:30")
    
    val res = ringing.processCel(cel)
    
    res.isInstanceOf[Answered] shouldBe true
    callData.ringDurationOnAnswer should equal(Some(7))
  }

  it should "return Answered and set ring duration on ANSWER with application AppQueue" in {
    val ringing = new Ringing(callData, cel)
    ringing.ringStartTime = format.parseDateTime("2014-05-23 14:05:23")
    cel.eventType = "ANSWER"
    cel.appName = "AppQueue"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:30")

    val res = ringing.processCel(cel)

    res.isInstanceOf[Answered] shouldBe true
    callData.ringDurationOnAnswer should equal(Some(7))
  }

  it should "return Started on HANGUP with AppDial" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    val ringing = new Ringing(callData, cel)
      
    ringing.processCel(cel).isInstanceOf[Started] shouldBe true
  }

  it should "return agent id on HANGUP with AppDial" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    cel.exten = "id-7"
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.agentId shouldEqual Some(7)
  }

  it should "return none on incomplete agent id on HANGUP with AppDial" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    cel.exten = "id"
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.agentId shouldEqual None
  }

  it should "return none on wrong agent id on HANGUP with AppDial" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    cel.exten = "hu-7"
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.agentId shouldEqual None
  }
  
  it should "return HangedUp on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    val ringing = new Ringing(callData, cel)
      
    ringing.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "set the dstNum if it has not been set previously" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    cel.cidNum = "1000"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")
    callData.srcNum = None
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.dstNum shouldEqual Some("1000")
  }

  it should "set the cidNum on ANSWER" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    cel.cidNum = "1000"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")
    callData.srcNum = None
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.cidNum shouldEqual Some("1000")
  }

  it should "set the cidNum on HANGUP" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    cel.cidNum = "1000"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")
    callData.srcNum = None
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.cidNum shouldEqual Some("1000")
  }
}