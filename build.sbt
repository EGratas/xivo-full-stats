import com.typesafe.sbt.packager.docker._
import org.clapper.sbt.editsource.EditSourcePlugin
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport.{edit, _}
import sbt.Keys.{libraryDependencies, mappings, run, scalaVersion}
import sbt.file


val appVersion:String = sys.env.get("TARGET_VERSION").getOrElse("2017.03.01")


lazy val root = (project in file("."))
  .enablePlugins(DockerPlugin)
  .enablePlugins(JavaServerAppPackaging)
  .settings(
    name := "xivo-full-stats",
    version := appVersion,
    scalaVersion := "2.12.8",
    mainClass in Compile := Some("xivo.fullstats.Main"),
    libraryDependencies ++= runDependencies ++ testDependencies,
    publishArtifact in (Compile, packageDoc) := false,
    resolvers += "Typesafe Repository" at "https://repo.typesafe.com/typesafe/releases/",
    testOptions in Test += Tests.Argument("-oD")
  )
  .settings(editSourceSettings: _*)
  .settings(testSettings: _*)
  .settings(compileSettings: _*)
  .settings(dockerSettings: _*)
  .settings(docSettings: _*)

lazy val runDependencies = Seq(
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
  "org.playframework.anorm" %% "anorm" % "2.6.2",
  "com.typesafe.play" %% "anorm-akka" % "2.5.3",
  "joda-time" % "joda-time" % "2.9.9",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe" % "config" % "1.3.4",
  "com.typesafe.play" %% "play-json" % "2.7.3",
  "com.typesafe.akka" %% "akka-stream" % "2.5.23",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.23"
)

lazy val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "org.dbunit" % "dbunit" % "2.4.7" % "test",
  "org.easymock" % "easymock" % "3.1" % "test",
  "org.mockito" % "mockito-all" % "1.10.19" % "test",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.23" % Test
)

lazy val dockerSettings = Seq (
  dockerCommands += Cmd("ENV", "DB_NAME xivo_stats"),
  dockerCommands += Cmd("ENV", "CHANGELOG_FILE db.changelog.xml"),
  dockerCommands += Cmd("ENV", "DB_PORT 5432"),
  dockerCommands += Cmd("ENV", "DB_HOST db"),
  dockerCommands += Cmd("ENV", "FORMAT xml"),
  dockerCommands += Cmd("ENV", "DB_USERNAME asterisk"),
  dockerCommands += Cmd("ENV", "DB_PASSWORD proformatique"),
  dockerCommands += Cmd("ENV", "LB_PATH /liquibase"),
  dockerCommands += Cmd("ADD", "./opt/docker/sqlscripts $LB_PATH/changelog"),

  dockerCommands += Cmd("LABEL", s"""version="${appVersion}""""),
  maintainer in Docker := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "xivoxc/xivo-liquibase:2023.02.latest",
  dockerRepository := Some("xivoxc"),
  dockerExposedVolumes := Seq("/var/log"),
  dockerEntrypoint := Seq("/opt/docker/bin/xivo-full-stats-docker"),
  daemonUserUid in Docker := None,
  daemonUser in Docker    := "daemon",
  dockerChmodType := DockerChmodType.UserGroupWriteExecute
)

val setVersionVarTask = Def.task {
    System.setProperty("XIVO_STATS_VERSION", appVersion)
}

lazy val editSourceSettings = Seq(
  flatten in EditSource := true,
  mappings in Universal += file("conf/appli.version") -> "conf/appli.version",
  mappings in Universal += file("src/main/resources/logback.xml") -> "conf/logback.xml",
  targetDirectory in EditSource := baseDirectory.value / "conf",
  variables in EditSource += ("SBT_EDIT_APP_VERSION", appVersion),
  (sources in EditSource) ++= (baseDirectory.value / "src/res" * "appli.version").get,
  edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value
)

lazy val testSettings = Seq(
  unmanagedResourceDirectories in Test ++= (baseDirectory.value / "src/universal/sqlscripts").get,
  parallelExecution in Test := false
)

lazy val compileSettings = Seq(
  packageBin in Compile := ((packageBin in Compile) dependsOn (edit in EditSource)).value,
  run in Compile := ((run in Compile) dependsOn setVersionVarTask).evaluated
)

lazy val docSettings = Seq(
  publishArtifact in(Compile, packageDoc) := false,
  publishArtifact in packageDoc := false,
  sources in(Compile, doc) := Seq.empty
)
